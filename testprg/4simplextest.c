#include "library.h"


int main(){

  //////////////////// Boundary Spins and Dl ////////////////////

  unsigned int two_j1, two_j2, two_j3, two_j4, two_j5, two_j6, two_j7, two_j8, two_j9, two_j10;
  unsigned int two_i1, two_i2, two_i3, two_i4, two_i5;

  two_j5 = 2; two_j6 = 2;
  two_j7 = 2; two_j8 = 2;
  two_i4 = 2;

  unsigned int two_Dl = 2;

  //////////////////// Immirzi Definition ////////////////////

  float Immirzi = 1.2;

  //////////////////// Data Folder Check ////////////////////

  Data_Folder_4 (Immirzi);

  //////////////////// Initialize wigxjpf and hash table////////////////////

  sl2cfoam_init();

  //////////////////// Start Computation ////////////////////

  unsigned int two_j = 2;

  double Sl2cValue = FourSimplex (two_j, two_j, two_j, two_j, two_j,
                                  two_j, two_j, two_j, two_j, two_j,
                                  0, 0, 0, 0, 0,
                                  two_Dl, Immirzi);

  if (fabs(Sl2cValue) > 1.0){
    printf ("Error while creating the library. Make clean and re-make");
    return 0;
  }

  printf("\nEPRL 4-Simplex: 2 2 2 2 2 2 2 2 2 2 0 0 0 0 0 | %i | %17g \n\n",
        two_Dl,
        Sl2cValue);

  //////////////////// Free Library ////////////////////

  sl2cfoam_free();

  return 0;
}
