//gcc-8 -std=c99 -fopenmp -Iwigxjpf-1.7/inc/ -Isrc/ -Iinc/ testprg/coherentest.c -Lwigxjpf-1.7/lib/ -Llib/ -lsl2cfoam -fopenmp -lgsl -lgslcblas -lmpfr -lmpc -lgmp -lwigxjpf -lm -lsl2cfoam -o bin/coherentest

#include "library.h"

int main(){

  ////////////////////////Spins//////////////////////

  int two_j1=2,two_j2=2,two_j3=2,two_j4=2;

  ////////////////////////Check values and triangular inequalities//////////////////////

  if (two_j4 > two_j1+two_j2+two_j3)
  {
      return 0;
  }

  unsigned int limi1= max(abs(two_j1-two_j2),abs(two_j3-two_j4));
  unsigned int limi2 = min(two_j1+two_j2, two_j3+ two_j4);

  //////////////////////// Define Normals //////////////////////

  double f1,f2,f3,f4,t1,t2,t3,t4;

  f1 = 0;
  t1 = 0;
  f2 = 0;
  t2 = 1.9106332362490186;
  f3 = 2.0943951023931953;
  t3 = 1.9106332362490186;
  f4 = -2.0943951023931953;
  t4 = 1.9106332362490186;

  //////////////////////// Initialize  the library //////////////////////

  sl2cfoam_init();

  /////////////////////////////////////////
  //Coherent State  - Creation and printing
  /////////////////////////////////////////

  for (unsigned int two_i = limi1; two_i <= limi2; two_i+=2){

    double complex coherentStatei = 0. + 0.*I;

    coherentStatei = CoherentStateI(two_i, two_j1, two_j2, two_j3, two_j4,
                                     m_pi+(f1), m_pi-(t1),
                                     m_pi+(f2), m_pi-(t2),
                                     m_pi+(f3), m_pi-(t3),
                                     m_pi+(f4), m_pi-(t4),
                                     -1,-1,-1,-1);

    printf("%g | %17g %17g*I \n", (float)(two_i)/2,creal(coherentStatei),cimag(coherentStatei));

  }


  //////////////////////// Deallocate memory //////////////////////

  sl2cfoam_free();

}
