#include "library.h"

int main(){

  //////////////////// Boundary Spins and Dl ////////////////////

  unsigned int two_j1, two_j2, two_j3, two_j4, two_j5, two_j6;

  two_j1 = 4; two_j2 = 6;
  two_j3 = 4; two_j4 = 6;
  two_j5 = 4; two_j6 = 4;

  unsigned int two_Dl = 20;

  //////////////////// Immirzi Definition ////////////////////

  float Immirzi = 1.2;

  //////////////////// Data Folder Check ////////////////////

  Data_Folder_3 (Immirzi);

  //////////////////// Initialize library ////////////////////

  sl2cfoam_init();

  //////////////////// Start Computation ////////////////////

  double value = ThreeSimplex ( two_j1, two_j2, two_j3,
                                two_j4, two_j5, two_j6,
                                two_Dl, Immirzi);
  if (fabs(value) > 1.0){
    printf ("Error while creating the library. Make clean and re-make");
    return 0;
  }

  printf("EPRL 3-Simplex: %i %i %i %i %i %i | %i | %17g \n",
        two_j1, two_j2, two_j3, two_j4, two_j5, two_j6, two_Dl, value);

  //////////////////// Free Library ////////////////////

  sl2cfoam_free();

  return 0;
}
