//gcc-7 -std=c99 -fopenmp -Iwigxjpf-1.7/inc/ -Isrc/ -Iinc/ test/b4timingL.c -Lwigxjpf-1.7/lib/ -Llib/ -lsl2cfoam -fopenmp -lgsl -lgslcblas -lmpfr -lmpc -lgmp -lwigxjpf -lm -lsl2cfoam -o bin/b4timingL

#include "library.h"
#include <time.h>

int main(int argc, char **argv){

  //////////////////// Boundary Spins and Dl ////////////////////

  unsigned int two_j =2 ;
  unsigned int two_Dl;
  float Immirzi =1.2;

  //////////////////// Initialize library////////////////////

  sl2cfoam_init();

  for(two_Dl = 0; two_Dl<= 20; two_Dl+=2){

    clock_t begin = clock();

    B4_Hash       ( two_j, two_j, two_j,
                    two_j, two_j, two_j,
                    two_j, two_j, two_j,
                    two_j, two_Dl,
                    Immirzi);

    clock_t end = clock();
    double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("{%i, %g},", two_Dl, time_spent);

  }
  //////////////////// Free Library ////////////////////

  sl2cfoam_free();

  return 0;
}
