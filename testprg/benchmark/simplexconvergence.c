//gcc-7 -std=c99 -fopenmp -Iwigxjpf-1.7/inc/ -Isrc/ -Iinc/ test/simplexconvergence.c -Lwigxjpf-1.7/lib/ -Llib/ -lsl2cfoam -fopenmp -lgsl -lgslcblas -lmpfr -lmpc -lgmp -lwigxjpf -lm -lsl2cfoam -o bin/simplexconvergence

#include "library.h"
#include <time.h>

int main(){

  unsigned int two_j=2;

  //////////////////// Immirzi Definition ////////////////////

  float Immirzi = 1.2;

  //////////////////// Data Folder Check ////////////////////

  Data_Folder_4 (Immirzi);

  //////////////////// Initialize wigxjpf and hash table////////////////////

  sl2cfoam_init();

  //////////////////// Start Computation ////////////////////
  double Sl2cValue;

  for(int two_Dl = 0; two_Dl <= 24; two_Dl+=2){

    Sl2cValue= FourSimplex (two_j, two_j, two_j, two_j, two_j,
                            two_j, two_j, two_j, two_j, two_j,
                            2, 2, 4 ,4, 4,
                            two_Dl, Immirzi);

    printf("{%i, %g},", two_Dl, Sl2cValue);

  }

  //////////////////// Free Library ////////////////////

  sl2cfoam_free();

  return 0;
}
