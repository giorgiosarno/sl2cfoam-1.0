//gcc-7 -std=c99 -fopenmp -Iwigxjpf-1.7/inc/ -Isrc/ -Iinc/ test/simplextimingCore.c -Lwigxjpf-1.7/lib/ -Llib/ -lsl2cfoam -fopenmp -lgsl -lgslcblas -lmpfr -lmpc -lgmp -lwigxjpf -lm -lsl2cfoam -o bin/simplextimingCore

#include "library.h"
#include <sys/time.h>

int main(int argc, char **argv){

  //////////////////// Boundary Spins and Dl ////////////////////

  unsigned int two_j = 2 ;
  unsigned int two_Dl = 24;
  float Immirzi = 1.2;

  Data_Folder_4 (Immirzi);

  struct timeval t1, t2;
  double elapsedTime;

  //////////////////// Initialize library////////////////////

  sl2cfoam_init();

  for(int i = atoi(argv[1]); i<= atoi(argv[2]); i++){

    omp_set_num_threads (i);
    gettimeofday(&t1, NULL);

    FourSimplex (two_j, two_j, two_j, two_j, two_j,
                two_j, two_j, two_j, two_j, two_j,
                two_j, two_j, two_j, two_j, two_j,
                two_Dl, Immirzi);

    gettimeofday(&t2, NULL);
    elapsedTime = (t2.tv_sec - t1.tv_sec) ;
    printf("{%i, %g},", i, elapsedTime);

  }
  //////////////////// Free Library ////////////////////

  sl2cfoam_free();

  return 0;
}
