//gcc-7 -std=c99 -fopenmp -Iwigxjpf-1.7/inc/ -Isrc/ -Iinc/ test/simplextimingL.c -Lwigxjpf-1.7/lib/ -Llib/ -lsl2cfoam -fopenmp -lgsl -lgslcblas -lmpfr -lmpc -lgmp -lwigxjpf -lm -lsl2cfoam -o bin/simplextimingL

#include "library.h"
#include <time.h>

int main(){

  unsigned int two_j=2;

  //////////////////// Immirzi Definition ////////////////////

  float Immirzi = 1.2;

  //////////////////// Data Folder Check ////////////////////

  Data_Folder_4 (Immirzi);

  //////////////////// Initialize wigxjpf and hash table////////////////////

  sl2cfoam_init();

  //////////////////// Start Computation ////////////////////
  double Sl2cValue;

  for(int two_Dl = 2; two_Dl <= 24; two_Dl+=2){

    clock_t begin = clock();

    Sl2cValue= FourSimplex (two_j, two_j, two_j, two_j, two_j,
                            two_j, two_j, two_j, two_j, two_j,
                            two_j, two_j, two_j, two_j, two_j,
                            two_Dl, Immirzi);

    clock_t end = clock();
    double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("{%i, %g},", two_Dl, time_spent);

  }

  //////////////////// Free Library ////////////////////

  sl2cfoam_free();

  return 0;
}
