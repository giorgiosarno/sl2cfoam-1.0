//gcc-7 -std=c99 -fopenmp-Iwigxjpf-1.7/inc/ -Isrc/ -Iinc/ test/simplextiming.c -Lwigxjpf-1.7/lib/ -Llib/ -lsl2cfoam -fopenmp -lgsl -lgslcblas -lmpfr -lmpc -lgmp -lwigxjpf -lm -lsl2cfoam -o bin/simplextiming

#include "library.h"
#include <time.h>


int main(){

  unsigned int two_Dl = 0;

  //////////////////// Immirzi Definition ////////////////////

  float Immirzi = 1.2;

  //////////////////// Data Folder Check ////////////////////

  Data_Folder_4 (Immirzi);

  //////////////////// Initialize wigxjpf and hash table////////////////////

  sl2cfoam_init();

  //////////////////// Start Computation ////////////////////
  double Sl2cValue;
  unsigned int two_j;

  for(two_j = 5; two_j <= 40; two_j+=2){

    clock_t begin = clock();

    Sl2cValue = FourSimplex (two_j, two_j, two_j, two_j, two_j,
                            two_j, two_j, two_j, two_j, two_j,
                            two_j, two_j, two_j, two_j, two_j,
                            two_Dl, Immirzi);

    clock_t end = clock();
    double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("{%i, %g},", two_j, time_spent);
    
  }

  //////////////////// Free Library ////////////////////

  sl2cfoam_free();

  return 0;
}
