//gcc-7 -std=c99 -fopenmp -Iwigxjpf-1.7/inc/ -Isrc/ -Iinc/ test/b4timingCore.c -Lwigxjpf-1.7/lib/ -Llib/ -lsl2cfoam -fopenmp -lgsl -lgslcblas -lmpfr -lmpc -lgmp -lwigxjpf -lm -lsl2cfoam -o bin/b4timingCore

#include "library.h"
#include <sys/time.h>

int main(int argc, char **argv){

  //////////////////// Boundary Spins and Dl ////////////////////

  unsigned int two_j = 20 ;
  unsigned int two_Dl = 0;
  float Immirzi = 1.2;
  float ImmirziSave = trunc(100 * Immirzi) / 100;

  struct timeval t1, t2;
  double elapsedTime;

  //////////////////// Initialize library////////////////////

  sl2cfoam_init();

  for(int i = atoi(argv[1]); i<= atoi(argv[2]); i++){

    omp_set_num_threads (i);

    wig_table_init(2*500, 6);
    #pragma omp parallel
    wig_temp_init(2*500);

    char pathRead[200];
    char pathRead1[200];
    sprintf(pathRead, "../data/4simplex/Immirzi_%.2f/HashTablesBooster/%i.%i.%i.%i_%i.boost",
            ImmirziSave,two_j, two_j, two_j, two_j, two_Dl);
    sprintf(pathRead1, "../data/4simplex/aux/Immirzi_%.2f/HashTablesKeyBooster/%i.%i.%i.%i_%i.kboost",
            ImmirziSave,two_j, two_j, two_j, two_j, two_Dl);

    remove(pathRead) == 0;
    remove(pathRead1) == 0;

    gettimeofday(&t1, NULL);

    B4_Hash       ( two_j, two_j, two_j,
                    two_j, two_j, two_j,
                    two_j, two_j, two_j,
                    two_j, two_Dl,
                    Immirzi);

    gettimeofday(&t2, NULL);
    elapsedTime = (t2.tv_sec - t1.tv_sec) ;
    printf("{%i, %g},", i, elapsedTime);

  }
  //////////////////// Free Library ////////////////////

  sl2cfoam_free();

  return 0;
}
