//gcc-7 -std=c99 -fopenmp -Iwigxjpf-1.7/inc/ -Isrc/ -Iinc/ test/15jtiming.c -Lwigxjpf-1.7/lib/ -Llib/ -lsl2cfoam -fopenmp -lgsl -lgslcblas -lmpfr -lmpc -lgmp -lwigxjpf -lm -lsl2cfoam -o bin/15jtiming

#include "library.h"
#include <time.h>

int main(int argc, char **argv){

  //////////////////// Boundary Spins and Dl ////////////////////

  unsigned int two_j;
  unsigned int two_Dl = atoi(argv[1]);
  unsigned int two_i;

  //////////////////// Initialize library////////////////////

  sl2cfoam_init();

  for(two_j = 0; two_j<= 40; two_j++){

    clock_t begin = clock();

    J15Symbol_Hash( two_j, two_j, two_j,
                    two_j, two_j, two_j,
                    two_j, two_j, two_j,
                    two_j, two_j, two_Dl );

    clock_t end = clock();
    double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("{%i, %g},", two_j, time_spent);

  }
  //////////////////// Free Library ////////////////////

  sl2cfoam_free();

  return 0;
}
