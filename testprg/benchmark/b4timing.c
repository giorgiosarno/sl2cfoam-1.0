//gcc-7 -std=c99 -fopenmp -Iwigxjpf-1.7/inc/ -Isrc/ -Iinc/ test/b4timing.c -Lwigxjpf-1.7/lib/ -Llib/ -lsl2cfoam -fopenmp -lgsl -lgslcblas -lmpfr -lmpc -lgmp -lwigxjpf -lm -lsl2cfoam -o bin/b4timing

#include "library.h"
#include <time.h>

int main(int argc, char **argv){

  //////////////////// Boundary Spins and Dl ////////////////////

  unsigned int two_j ;
  unsigned int two_Dl = atoi(argv[1]);
  float Immirzi =1.2;

  //////////////////// Initialize library////////////////////

  sl2cfoam_init();

  for(two_j = 40; two_j<= 40; two_j++){


  clock_t begin = clock();

  B4_Hash       ( two_j, two_j, two_j,
                  two_j, two_j, two_j,
                  two_j, two_j, two_j,
                  two_j, two_Dl,
                  Immirzi);

  clock_t end = clock();
  double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
  printf("{%i, %g},", two_j, time_spent);

  }
  //////////////////// Free Library ////////////////////

  sl2cfoam_free();

  return 0;
}
