/* Copyright 2018 Giorgio Sarno and Pietro Donà*/

/* EPRL is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   EPRL is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.*/

#ifndef __LIBRARY_H__
#define __LIBRARY_H__

#include "../src/common.h"
#include "../src/jsymbols.h"
#include "../src/b4function.h"
#include "../src/b3function.h"
#include "../src/recouplingsl2c.h"
#include "../src/coherentstates.h"
#include "../src/amplitude.h"

////////////////////////////////////////////////////////////////////////
// Library for EPRL vertex amplitude
////////////////////////////////////////////////////////////////////////

// Always call this function before using the library.
void sl2cfoam_init(){
	init_wigxjpf();
	init_complex_gamma();
}

// Always call this function after using the library.
void sl2cfoam_free(){
	clear_wigxjpf();
	clear_complex_gamma();

};


#endif/*__LIBRARY__*/
