/* Copyright 2018 Giorgio Sarno and Pietro Donà*/

/* sl2cfoam is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   sl2cfoam is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.*/


#include "amplitude.h"

////////////////////////////////////////////////////////
////////////////////////////////////////////////////////
////////////////////////////////////////////////////////

double FourSimplex (unsigned int two_j1, unsigned int two_j2, unsigned int two_j3, unsigned int two_j4,
                    unsigned int two_j5, unsigned int two_j6, unsigned int two_j7, unsigned int two_j8,
                    unsigned int two_j9, unsigned int two_j10,
                    unsigned int two_i1, unsigned int two_i2, unsigned int two_i3,
                    unsigned int two_i4, unsigned int two_i5,
                    unsigned int two_Dl, float Immirzi) {

  //////////////////// Data Folder Check ////////////////////

  Data_Folder_4 (Immirzi);

  //////////////////// Data Folder Check ////////////////////

  float ImmirziSave = trunc(100 * Immirzi) / 100;
  char pathRead[200];
  char pathRead1[200];
  sprintf(pathRead, "../data/4simplex/Immirzi_%.2f/HashTablesEPRL/%i.%i.%i.%i_%i_%i.eprl",
          ImmirziSave, two_j5, two_j6, two_j7, two_j8, two_i4, two_Dl);

  //////////////////// Initialize or Load hash table////////////////////

  khash_t(HashTableEPRL) *h3 = NULL;

  sprintf(pathRead1, "../data/4simplex/Immirzi_%.2f/HashTablesEPRL/%i.%i.%i.%i_%i_%i.eprl",
          ImmirziSave, two_j5, two_j6, two_j7, two_j8, two_i4, two_Dl);
  if(file_exist (pathRead1) != 0 ){
    h3 = kh_load(HashTableEPRL, pathRead1);
  }
  else{
      h3 = kh_init(HashTableEPRL);
  }

  //////////////////// Do we already have the data? ////////////////////

  HashTableEPRL_key_t keyEPRL = { two_j1, two_j2, two_j3, two_j4, two_j7, two_j8,
                                  two_i1, two_i2, two_i3, two_i5};

  if (kh_get(HashTableEPRL, h3, keyEPRL) != kh_end(h3)){
    khint_t s = kh_get(HashTableEPRL, h3, keyEPRL);
    #if PRINT_DATA
    printf("\nEPRL 4-Simplex: %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i | %i | %17g \n\n",
          two_j1, two_j2, two_j3, two_j4, two_j5,
          two_j6, two_j7, two_j8, two_j9, two_j10,
          two_i1, two_i2, two_i3, two_i4, two_i5, two_Dl,
          kh_val(h3,s));
    #endif
    return kh_val(h3,s);
  }

  //////////////////// Prepare Hash Tables for 6j and B4 ////////////////////

  for (int i = 0; i <= two_Dl; i+=2){

    J15Symbol_Hash( two_j1, two_j2, two_j3,
                    two_j4, two_j5, two_j6,
                    two_j7, two_j8, two_j9,
                    two_j10, two_i4, i);
    B4_Hash( two_j1, two_j2, two_j3,
             two_j4, two_j5, two_j6,
             two_j7, two_j8, two_j9,
             two_j10, i,
             Immirzi);
  }

  //////////////////// Loading Hash Tables ////////////////////

  char pathRead2[200];
  char pathRead4[200];
  char pathRead5[200];

  sprintf(pathRead2,"../data/4simplex/HashTablesJ6/%i.%i.%i.%i_%i_%i.6j",
          two_j5, two_j6, two_j9, two_j10, two_i4, two_Dl);
  sprintf(pathRead4,"../data/4simplex/Immirzi_%.2f/HashTablesBooster/%i.%i.%i.%i_%i.boost",
          ImmirziSave, two_j5, two_j6, two_j9, two_j10, two_Dl);
  sprintf(pathRead5, "../data/4simplex/Immirzi_%.2f/HashTablesEPRL/%i.%i.%i.%i_%i_%i.eprl",
                  ImmirziSave, two_j5, two_j6, two_j9, two_j10, two_i4, two_Dl);

  khash_t(HashTableJ6) *h = kh_load(HashTableJ6, pathRead2);
  khash_t(HashTableBooster) *h2 = kh_load(HashTableBooster, pathRead4);

  //////////////// Initialize SL2C values and MPFR class ////////////////

  mpfr_rnd_t  rnd = GMP_RNDN;
  double Sl2cValue = 0.0;
  double err = 0.0;

  //////////////////// Booster CutOff ////////////////////

  double CutOff = pow(10,-40);
  //////////////////// Start L summations ////////////////////
  //I'm parallelizing on l's summations
  #pragma omp parallel reduction (+:Sl2cValue, err)
  {
    #pragma omp for collapse(6)
    for (unsigned int two_l1 = two_j1 ; two_l1 <= two_j1+two_Dl; two_l1+=2){
    for (unsigned int two_l2 = two_j2; two_l2 <= two_j2+two_Dl; two_l2+=2){
    for (unsigned int two_l3 = two_j3; two_l3 <= two_j3+two_Dl; two_l3+=2){
    for (unsigned int two_l4 = two_j4; two_l4 <= two_j4+two_Dl; two_l4+=2){
    for (unsigned int two_l7 = two_j7; two_l7 <= two_j7+two_Dl; two_l7+=2){
    for (unsigned int two_l8 = two_j8; two_l8 <= two_j8+two_Dl; two_l8+=2){

      //////////////////// Range for internal K intertwiners ////////////////////

      unsigned int  two_limk2_1, two_limk2_2,two_limk3_1, two_limk3_2,
                    two_limk5_1, two_limk5_2;

      two_limk2_1 = max(abs(two_j10-two_l7),abs(two_l1-two_l2));
      two_limk2_2 = min(two_j10+two_l7,two_l1+two_l2);
      two_limk3_1 = max(abs(two_j9-two_l8),abs(two_l3-two_l1));
      two_limk3_2 = min(two_j9+two_l8,two_l3+two_l1);
      two_limk5_1 = max(abs(two_l4-two_j6),abs(two_l7-two_l8));
      two_limk5_2 = min(two_l4+two_j6,two_l7+two_l8);

      ///////////////// Initialize pointers for 6j keys and boosters /////////////////

      char *A = malloc(6*sizeof(int));

      khint_t hintBooster;
      double boosterB , boosterC, boosterE, boosterA;

      //////////////////// Start K summations ////////////////////

      for(unsigned int  two_k2 = two_limk2_1; two_k2 <= two_limk2_2; two_k2+=2){

        //////////////////// Get booster functions  ////////////////////

        //TODO Find a better and cache friendly way
        //     to get boosters data

        HashTableBooster_key_t keyBoosterB = {two_j10, two_j7, two_j1, two_j2,
                                              two_j10, two_l7, two_l1, two_l2,
                                              two_i2, two_k2};
        hintBooster = kh_get(HashTableBooster, h2, keyBoosterB);
        boosterB = d(two_i2)*d(two_k2)*kh_val(h2,hintBooster);
        //Apply K cutoffs
        if ( fabs(boosterB) < CutOff ) continue;

      for(unsigned int  two_k3 = two_limk3_1; two_k3 <= two_limk3_2; two_k3+=2){

        HashTableBooster_key_t keyBoosterC = {two_j9, two_j8, two_j3, two_j1,
                                              two_j9, two_l8, two_l3, two_l1,
                                              two_i3, two_k3};
        hintBooster = kh_get(HashTableBooster, h2, keyBoosterC);
        boosterC = d(two_i3)*d(two_k3)*kh_val(h2,hintBooster);

        if ( fabs(boosterC) < CutOff ) continue;

      for(unsigned int  two_k5 = two_limk5_1; two_k5 <= two_limk5_2; two_k5+=2){

          HashTableBooster_key_t keyBoosterE = {two_j4, two_j6, two_j7, two_j8,
                                                two_l4, two_j6, two_l7, two_l8,
                                                two_i5, two_k5};
          hintBooster = kh_get(HashTableBooster, h2, keyBoosterE);
          boosterE = d(two_i5)*d(two_k5)*kh_val(h2,hintBooster);

          if ( fabs(boosterE) < CutOff ) continue;

      for(unsigned int  two_k1 = max(max(abs(two_l3-two_l2),abs(two_j5-two_l4)),max(abs(two_k3-two_k2),abs(two_k5-two_i4)));
                        two_k1 <= min(min(two_l3+two_l2,two_j5+two_l4),min(two_k2+two_k3,two_k5+two_i4)); two_k1+=2){


      HashTableBooster_key_t keyBoosterA = {two_j2, two_j3, two_j5, two_j4,
                                            two_l2, two_l3, two_j5, two_l4,
                                            two_i1, two_k1};
      hintBooster = kh_get(HashTableBooster, h2, keyBoosterA);
      boosterA = d(two_i1)*d(two_k1)*kh_val(h2,hintBooster);

      if ( fabs(boosterA) < CutOff) continue;

      /////////////////////////////////////
      //Convert Boosters to MPFR variables
      /////////////////////////////////////

      mpfr_t  boosterMPFR_A, boosterMPFR_B, boosterMPFR_C, boosterMPFR_E, boostersMPFR ;

      mpfr_init_set_d (boosterMPFR_A, boosterA, GMP_RNDN);
      mpfr_init_set_d (boosterMPFR_B, boosterB, GMP_RNDN);
      mpfr_init_set_d (boosterMPFR_C, boosterC, GMP_RNDN);
      mpfr_init_set_d (boosterMPFR_E, boosterE, GMP_RNDN);
      mpfr_init (boostersMPFR);

      ///////////////////////////////////
      //Multiply Boosters as MPFR variables
      ///////////////////////////////////

      mpfr_mul (boostersMPFR, boosterMPFR_A, boosterMPFR_B, GMP_RNDN);
      mpfr_mul (boostersMPFR, boostersMPFR, boosterMPFR_C, GMP_RNDN);
      mpfr_mul (boostersMPFR, boostersMPFR, boosterMPFR_E, GMP_RNDN);

      mpfr_clears ( boosterMPFR_A, boosterMPFR_B,
                    boosterMPFR_C, boosterMPFR_E,
                    NULL);

      //////////////////// Compute 15j Symbol  ////////////////////

      double val15j = d(two_i4) * J15Symbol ( h, &A,
                                              two_l1, two_l2, two_l3, two_l4, two_j5,
                                              two_j6, two_l7, two_l8, two_j9, two_j10,
                                              two_i1, two_i2, two_i3, two_i4, two_i5,
                                              two_k1, two_k2, two_k3, two_k5
                                            );

       //printf ("%17g \n",val15j);
       //printf ("%17g \n", boosterA*boosterB*boosterC*boosterE);

       ///////////////////////////////////////////////////////////
       //Convert 15j symbol and coherent states to MPFR variables
       ///////////////////////////////////////////////////////////

        mpfr_t  val15jMPFR;
        mpfr_init_set_d (val15jMPFR, val15j, GMP_RNDN);

        ////////////////////////////////////////
        //Compute SL2C value as an MPFR variable
        ////////////////////////////////////////

        mpfr_t  Sl2cValueMPFR;

        mpfr_init (Sl2cValueMPFR);
        mpfr_mul (Sl2cValueMPFR, val15jMPFR, boostersMPFR, GMP_RNDN);

        /////////////////////////////////////////////////////////
        //Back to double and Kahan compensated summation for I,K
        ////////////////////////////////////////////////////////

        CompensatedSummationMPFR_EPRL (&err, &Sl2cValue, Sl2cValueMPFR);

        ////////////////////////////
        //Free MPFR variables memory
        ////////////////////////////

        mpfr_clears ( boostersMPFR, val15jMPFR,Sl2cValueMPFR, NULL);
      }
      }
      }
      }
    ///////////////////
    //Free 6j pointer
    //////////////////
    free(A);
    }
    }
    }
    }
    }
    }
  }

  //////////////////// Put key and value in the EPRL HashTable ////////////////////

  int ret;
  khint_t s;
  s = kh_put(HashTableEPRL, h3, keyEPRL, &ret);

  if ( ret == 1 ){
    kh_val(h3,s) = Sl2cValue;
    s=kh_get(HashTableEPRL, h3, keyEPRL);
    if( kh_val(h3,s) == 0 || kh_val(h3,s) != Sl2cValue){
      kh_val(h3,s) = Sl2cValue;
    }
  }
  #if PRINT_DATA
  //////////////////// Print the result  ////////////////////
  printf("\nEPRL 4-Simplex: %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i | %i | %17g \n\n",
        two_j1, two_j2, two_j3, two_j4, two_j5,
        two_j6, two_j7, two_j8, two_j9, two_j10,
        two_i1, two_i2, two_i3, two_i4, two_i5, two_Dl,
        Sl2cValue);
  #endif

  //////////////////// Save Data ////////////////////

  #if SAVE_DATA
  kh_write(HashTableEPRL, h3, pathRead);
  #endif

  //////////////////// Free Memory from HashTables ////////////////////

  kh_destroy(HashTableJ6, h);
  kh_destroy(HashTableBooster, h2);
  kh_destroy(HashTableEPRL, h3);

  return Sl2cValue;

  }

////////////////////////////////////////////////////////
////////////////////////////////////////////////////////
////////////////////////////////////////////////////////

// Prepare arrays to account for B3 and {6j}
static  double Boost3[3][200][200] = {0};
static  double ArrayJ6[200][200][200] = {0};

double ThreeSimplex ( unsigned int two_j1, unsigned int two_j2, unsigned int two_j3,
                      unsigned int two_j4, unsigned int two_j5, unsigned int two_j6,
                      unsigned int two_Dl, float Immirzi) {

  if( two_j5 % 2 != 0 && two_j4 % 2 != 0 && two_j1 % 2 != 0 ||
      two_j6 % 2 != 0 && two_j2 % 2 != 0 && two_j5 % 2 != 0 ||
      two_j3 % 2 != 0 && two_j4 % 2 != 0 && two_j6 % 2 != 0 ||
      two_j1 % 2 != 0 && two_j2 % 2 != 0 && two_j3 % 2 != 0){
    printf("Problem with triangular inequalities \n");
    exit(0);
  }

  //////////////////// Data Folder Check ////////////////////

  Data_Folder_3 (Immirzi);

  //////////////////// Data Folder Check ////////////////////

  float ImmirziSave =trunc(100 * Immirzi) / 100;
  char pathRead0[200];
  char pathRead00[200];
  sprintf(pathRead0, "../data/3simplex/Immirzi_%.2f/HashTablesEPRL/%i.%i.%i_%i.eprl",
          ImmirziSave, two_j1, two_j2, two_j3, two_Dl);

  //////////////////// Initialize wigxjpf and hash table////////////////////

  khash_t(HashTableEPRL3) *h2 = NULL;

  sprintf(pathRead00, "../data/3simplex/Immirzi_%.2f/HashTablesEPRL/%i.%i.%i_%i.eprl",
          ImmirziSave, two_j1, two_j2, two_j3, two_Dl);
  //Check already computed values or tables
  if(file_exist (pathRead00) != 0 ){
     h2 = kh_load(HashTableEPRL3, pathRead00);
  }
  else{
      h2 = kh_init(HashTableEPRL3);
  }


  HashTableEPRL3_key_t keyEPRL = {two_j4, two_j5, two_j6, two_Dl};

  if (kh_get(HashTableEPRL3, h2, keyEPRL) != kh_end(h2)){
    khint_t s = kh_get(HashTableEPRL3, h2, keyEPRL);
    #if PRINT_DATA
    printf("EPRL 3-Simplex: %i %i %i %i %i %i | %i | %17g \n",
          two_j1, two_j2, two_j3, two_j4, two_j5, two_j6, two_Dl, kh_val(h2,s));
    #endif
    return kh_val(h2,s);
  }

  double value;
  double value_1 = 0.0;
  mpfr_t value_Dl, value_Dl_1;
  mpfr_init_set_d (value_Dl_1, 0.0, GMP_RNDN);

  char pathRead[200];
  char pathRead1[200];
  char pathRead2[200];

  sprintf(pathRead, "../data/3simplex/HashTablesJ6/%i.%i.%i_%i.6j",
          two_j1, two_j2, two_j3, two_Dl);
  sprintf(pathRead1, "../data/3simplex/Immirzi_%.2f/HashTablesBooster/%i.%i.%i_%i.boost",
          ImmirziSave, two_j1, two_j2, two_j3, two_Dl);
  sprintf(pathRead2, "../data/3simplex/Immirzi_%.2f/HashTablesEPRL/%i.%i.%i_%i.boost",
          ImmirziSave,two_j1, two_j2, two_j3, two_Dl);

  //Initialize Hash Tables for B3 and {6j}
  khash_t(HashTableB3) *h = NULL;
  char pathRead_Check[200];
  for ( int i = two_Dl;  i >= 0; i-=2){
    sprintf(pathRead_Check, "../data/3simplex/Immirzi_%.2f/HashTablesBooster/%i.%i.%i_%i.boost",
            ImmirziSave, two_j1, two_j2, two_j3, i);
    if(file_exist (pathRead_Check) != 0 ){
        h = kh_load(HashTableB3, pathRead_Check);
        break;
    }
  }
  if ( h == NULL){
      h = kh_init(HashTableB3);
  }

  khash_t(HashTableJ6) *h1 = NULL;
  for ( int i = two_Dl;  i >= 0; i-=2){
    sprintf(pathRead_Check, "../data/3simplex/HashTablesJ6/%i.%i.%i_%i.6j",
            two_j1, two_j2, two_j3, i);
    if(file_exist (pathRead_Check) != 0 ){
        h1 = kh_load(HashTableJ6, pathRead_Check);
        break;
    }
  }
  if ( h1 == NULL){
      h1 = kh_init(HashTableJ6);
  }

  ///////////////////////////////////
  ///////////////////////////////////

  value = 0.0;
  double err = 0.0;
  double CutOff = pow(10,-30);

  ///////////////////////////////////
  //Keys for B3 and J6 into che
  //hash tables, these are all the
  //combination that we need
  ///////////////////////////////////

  for (unsigned int two_l4 = two_j4; two_l4 <= two_j4 + two_Dl; two_l4 +=2){
  for (unsigned int two_l5 = max (two_j5, abs(two_l4-two_j1));
                    two_l5 <= min(two_j5+two_Dl,two_l4+two_j1);
                    two_l5 +=2){
  for (unsigned int two_l6 = max(two_j6,max(abs(two_l4-two_j3),abs(two_l5-two_j2)));
                    two_l6 <= min(two_j6+two_Dl,min(two_l4+two_j3,two_l5+two_j2));
                    two_l6 +=2){
    //Set Keys
    HashTableB3_key_t keyA = {0, two_j5, two_j4, two_j1, two_l5, two_l4, two_j1};
    HashTableB3_key_t keyB = {1, two_j6, two_j2, two_j5, two_l6, two_j2, two_l5};
    HashTableB3_key_t keyC = {2, two_j3, two_j4, two_j6,two_j3, two_l4, two_l6};
    HashTableJ6_key_t keyJ6 = {two_l5,two_l4,two_j1,two_j3,two_j2,two_l6};

    int retA, retB, retC, retJ6;
    //If the key is absent create a new key
    if (kh_get(HashTableB3, h, keyA) == kh_end(h)){
      khint_t kA = kh_put(HashTableB3, h, keyA, &retA);
      kh_val(h,kA) = 0.0;
    }
    if (kh_get(HashTableB3, h, keyB) == kh_end(h)){
      khint_t kB = kh_put(HashTableB3, h, keyB, &retB);
      kh_val(h,kB) = 0.0;
    }
    if (kh_get(HashTableB3, h, keyC) == kh_end(h)){
      khint_t kC = kh_put(HashTableB3, h, keyC, &retC);
      kh_val(h,kC) = 0.0;
    }
    //Put keys for J6
    if (kh_get(HashTableJ6, h1, keyJ6) == kh_end(h1)){
      khint_t kJ6 = kh_put(HashTableJ6, h1, keyJ6, &retJ6);
      kh_val(h1,kJ6) == 0.0;
    }
  }
  }
  }

  ///////////////////////////////////
  //Compute J6  and store them
  //in ArrayJ6 as well as in the HashTable
  ///////////////////////////////////

  #pragma omp parallel for
  for (khint_t i = kh_begin(h1); i < kh_end(h1); i++) {
    if (!kh_exist(h1,i)) continue;

    HashTableJ6_key_t j6_key = kh_key(h1,i);
    unsigned int  two_l5_Aux, two_l4_Aux, two_j1_Aux,
                  two_j3_Aux, two_j2_Aux, two_l6_Aux;

    two_l5_Aux = j6_key.k[0];
    two_l4_Aux = j6_key.k[1];
    two_j1_Aux = j6_key.k[2];
    two_j3_Aux = j6_key.k[3];
    two_j2_Aux = j6_key.k[4];
    two_l6_Aux = j6_key.k[5];

    if (fabs(kh_val(h1,i)) < CutOff){
      ArrayJ6[two_l5_Aux-two_j5][two_l4_Aux-two_j4][two_l6_Aux-two_j6] =
                                  wig6jj(two_l5_Aux,two_l4_Aux,two_j1,
                                         two_j3,two_j2,two_l6_Aux);
      kh_val(h1,i) = ArrayJ6[two_l5_Aux-two_j5][two_l4_Aux-two_j4][two_l6_Aux-two_j6];
    }
    else{
      ArrayJ6[two_l5_Aux-two_j5][two_l4_Aux-two_j4][two_l6_Aux-two_j6] = kh_val(h1,i);
    }
  }

  kh_write(HashTableJ6, h1, pathRead);

  ///////////////////////////////////
  //Compute B3 functions and store them
  //in B3Array as well as in the HashTable
  //Parallelization is on
  ///////////////////////////////////

  #pragma omp parallel for
  for (khint_t i = kh_begin(h); i < kh_end(h); i++) {
    if (!kh_exist(h,i)) continue;

    HashTableB3_key_t boost_key = kh_key(h,i);
    unsigned int tetra_aux, two_j1_Aux, two_j2_Aux, two_j3_Aux,
                            two_l1_Aux, two_l2_Aux, two_l3_Aux;

    tetra_aux = boost_key.k[0];
    two_j1_Aux = boost_key.k[1];
    two_j2_Aux = boost_key.k[2];
    two_j3_Aux = boost_key.k[3];
    two_l1_Aux = boost_key.k[4];
    two_l2_Aux = boost_key.k[5];
    two_l3_Aux = boost_key.k[6];

    //Three possible boosted triangle, 0,1,2
    //they allhave one l spin fixed
    //by not boosted tetrahedra
    if(tetra_aux == 0){
      if (fabs(kh_val(h,i)) < CutOff){

        Boost3[0][two_l1_Aux-two_j1_Aux][two_l2_Aux-two_j2_Aux] =
                                        B3 ( two_j1_Aux, two_j2_Aux, two_j3_Aux,
                                             two_l1_Aux, two_l2_Aux, two_l3_Aux,
                                             Immirzi);
        kh_val(h,i) = Boost3[0][two_l1_Aux-two_j1_Aux][two_l2_Aux-two_j2_Aux] ;
      }
      else{
        Boost3[0][two_l1_Aux-two_j1_Aux][two_l2_Aux-two_j2_Aux] = kh_val(h,i);
      }
    }

    if(tetra_aux == 1){
      if (fabs(kh_val(h,i)) < CutOff){

        Boost3[1][two_l1_Aux-two_j1_Aux][two_l3_Aux-two_j3_Aux] =
                                        B3 ( two_j1_Aux, two_j2_Aux, two_j3_Aux,
                                             two_l1_Aux, two_l2_Aux, two_l3_Aux,
                                             Immirzi);
        kh_val(h,i) =  Boost3[1][two_l1_Aux-two_j1_Aux][two_l3_Aux-two_j3_Aux];
      }
      else{
        Boost3[1][two_l1_Aux-two_j1_Aux][two_l3_Aux-two_j3_Aux] = kh_val(h,i);
      }
    }
    if(tetra_aux == 2){
      if (fabs(kh_val(h,i)) < CutOff){

        Boost3[2][two_l2_Aux-two_j2_Aux][two_l3_Aux-two_j3_Aux] =
                                        B3 ( two_j1_Aux, two_j2_Aux, two_j3_Aux,
                                             two_l1_Aux, two_l2_Aux, two_l3_Aux,
                                             Immirzi);
        kh_val(h,i) = Boost3[2][two_l2_Aux-two_j2_Aux][two_l3_Aux-two_j3_Aux] ;
      }
      else{
        Boost3[2][two_l2_Aux-two_j2_Aux][two_l3_Aux-two_j3_Aux] = kh_val(h,i);
      }
    }
  }

  kh_write(HashTableB3, h, pathRead1);

  ///////////////////////////////////
  //Now we have all elements, start
  //to assembly the value and summing
  ///////////////////////////////////

  #pragma omp parallel for reduction (+:value, err)
  for (unsigned int two_l4 = two_j4; two_l4 <= two_j4 + two_Dl; two_l4 +=2){
    for (unsigned int two_l5 = max (two_j5,abs(two_l4-two_j1));
                      two_l5 <= min(two_j5 + two_Dl,two_l4+two_j1);
                      two_l5 +=2){
      //Retrieve B3_A

      double BoostA = Boost3[0][two_l5-two_j5][two_l4-two_j4];

      if ( fabs(BoostA) < CutOff ) continue;

      for (unsigned int two_l6 = max(two_j6, max (abs(two_l4-two_j3),abs(two_l5-two_j2)));
                        two_l6 <= min(two_j6 + two_Dl, min (two_l4+two_j3, two_l5+two_j2));
                        two_l6 +=2){
        //Retrieve B3_B,B3_C
        double BoostB = Boost3[1][two_l5-two_j5][two_l6-two_j6];
        double BoostC = Boost3[2][two_l4-two_j4][two_l6-two_j6];
        //printf("%17g %17g %17g \n", BoostA,BoostB,BoostC);

        if ( fabs(BoostB) < CutOff || fabs(BoostC) < CutOff ) continue;

        double J6 = ArrayJ6[two_l5-two_j5][two_l4-two_j4][two_l6-two_j6];
        mpfr_t  boosterMPFR_A, boosterMPFR_B, boosterMPFR_C, J6MPFR  ;
        //multiply values as mpfr variables
        mpfr_init_set_d (boosterMPFR_A, BoostA, GMP_RNDN);
        mpfr_init_set_d (boosterMPFR_B, BoostB, GMP_RNDN);
        mpfr_init_set_d (boosterMPFR_C, BoostC, GMP_RNDN);
        mpfr_init_set_d (J6MPFR, J6, GMP_RNDN);

        mpfr_mul(boosterMPFR_A,boosterMPFR_A,boosterMPFR_B,GMP_RNDN);
        mpfr_mul(boosterMPFR_A,boosterMPFR_A,boosterMPFR_C,GMP_RNDN);
        mpfr_mul(boosterMPFR_A,boosterMPFR_A,J6MPFR,GMP_RNDN);
        //Sum via compensated summation
        CompensatedSummationMPFR_EPRL (&err, &value, boosterMPFR_A);
        //printf("%i %17g  \n",two_Dl,value);
      }
    }
  }

  //////////////////// Put key and value in the EPRL3 HashTable ////////////////////

  int ret;
  khint_t s;
  s = kh_put(HashTableEPRL3, h2, keyEPRL, &ret);

  if ( ret == 1 ){
    kh_val(h2,s) = value;
    s=kh_get(HashTableEPRL3, h2, keyEPRL);
    if( kh_val(h2,s) == 0 || kh_val(h2,s) != value){
      kh_val(h2,s) = value;
    }
  }

  //////////////////// Print the result  ////////////////////

  #if PRINT_DATA
  printf("EPRL 3-Simplex: %i %i %i %i %i %i | %i | %17g \n",
        two_j1, two_j2, two_j3, two_j4, two_j5, two_j6, two_Dl, value);
  #endif

  //////////////////// Save Data ////////////////////

  #if SAVE_DATA
  kh_write(HashTableEPRL3, h2, pathRead0);
  #endif


  //Free memory
  kh_destroy(HashTableB3, h);
  kh_destroy(HashTableJ6, h1);
  kh_destroy(HashTableEPRL3, h2);

  return value;
}
