/* Copyright 2018 Giorgio Sarno and Pietro Donà */

/* sl2cfoam is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   sl2cfoam is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.*/


#include "recouplingsl2c.h"

////////////////////////////////////////////////////////
////////////////////////////////////////////////////////
////////////////////////////////////////////////////////

double  wigner_3j_sl2c ( unsigned int two_j1, unsigned int two_j2, unsigned int two_j3,
                         unsigned int two_k1, unsigned int two_k2, unsigned int two_k3,
                         float two_rho1, float two_rho2, float two_rho3){

   assert (two_j1 >= two_k1);
   assert (two_j2 >= two_k2);
   assert (two_j3 >= two_k3);

  double CutOff = pow(10,-20);
  int phase = real_negpow(two_j1 -two_j2 +two_j3);
  double value;
  value = phase*sqrt(d(two_j3))* X (two_j1, two_j2, two_j3,
                                    two_k1, two_k2, two_k3,
                                    two_rho1, two_rho2, two_rho3);
  if (fabs(value)<CutOff){
    return 0.0;
  }
  else{
    return value;
  }

}

double  wigner_6j_sl2c ( unsigned int two_k1, unsigned int two_k2, unsigned int two_k3,
                         unsigned int two_k4, unsigned int two_k5, unsigned int two_k6,
                         float two_rho1, float two_rho2, float two_rho3,
                         float two_rho4, float two_rho5, float two_rho6,
                         unsigned int two_Dj){

  assert (two_Dj >= two_k1);
  assert (two_Dj >= two_k2);
  assert (two_Dj >= two_k3);
  assert (two_Dj >= two_k4);
  assert (two_Dj >= two_k5);
  assert (two_Dj >= two_k6);

  double value = 0.0 + 0.0*I;
  double CutOff = pow(10,-25);

  #pragma omp parallel reduction (+:value)
  {
    #pragma omp for collapse(2)
    for (unsigned int two_j1 = two_k1; two_j1 <= two_Dj; two_j1 +=2 ){
    for (unsigned int two_j2 = two_k2; two_j2 <= two_Dj; two_j2 +=2 ){
    for (unsigned int two_j3 = max(two_k3, abs(two_j1 - two_j2));
                      two_j3 <= min(two_Dj , two_j1 + two_j2); two_j3 +=2 ){
    for (unsigned int two_j4 = two_k4; two_j4 <= two_Dj; two_j4 +=2 ){
    for (unsigned int two_j5 = max (two_k5, abs(two_j4-two_j3));
                      two_j5 <= min (two_Dj , two_j4+two_j3); two_j5 +=2 ){
    for (unsigned int two_j6 = max(abs(two_j4-two_j2), max(two_k6, abs(two_j1 - two_j5)));
                      two_j6 <= min(two_j4+two_j2, min ( two_Dj ,two_j1 + two_j5)); two_j6 +=2 ){

      double complex wig6j, wig1,wig2,wig3,wig4;

      wig6j = wig6jj(two_j1,two_j2,two_j3,two_j4,two_j5,two_j6);

      wig1 =  wigner_3j_sl2c (two_j1, two_j2, two_j3,
                              two_k1, two_k2, two_k3,
                              two_rho1, two_rho2, two_rho3);
      if (fabs(wig1)<CutOff) continue;
      wig2 =  wigner_3j_sl2c (two_j1, two_j5, two_j6,
                              two_k1, two_k5, two_k6,
                              two_rho1, two_rho5, two_rho6);
      if (fabs(wig2)<CutOff) continue;

      wig3 =  wigner_3j_sl2c (two_j4, two_j2, two_j6,
                              two_k4, two_k2, two_k6,
                              two_rho4, two_rho2, two_rho6);
      if (fabs(wig3)<CutOff) continue;

      wig4 =  wigner_3j_sl2c (two_j4, two_j5, two_j3,
                              two_k4, two_k5, two_k3,
                              two_rho4, two_rho5, two_rho3);
      if (fabs(wig4)<CutOff) continue;

      value +=  wig6j*wig1*wig2*wig3*wig4;

    }
    }
    }
    }
    }
    }
  }

  if (fabs(value)<CutOff ){
    return 0.0;
  }
  if (fabs(value)>1.0 ){
    printf("\nPrecision problem: too high spins.\n");
  }
  else{
    return value;
  }

}
