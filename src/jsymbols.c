/* Copyright 2018 Giorgio Sarno and Pietro Donà */

/* sl2cfoam is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   sl2cfoam is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.*/

/*sl2cfoam uses and modify WIGXJPF, Copyright 2015 Haakan T. Johansson
 	All details can be found at https://arxiv.org/abs/1504.08329*/


#include "jsymbols.h"

////////////////////////////////////////////////////////
////////////////////////////////////////////////////////
////////////////////////////////////////////////////////

#define MAX_J 500

bool was_wig_init = false;

void init_wigxjpf() {

	wig_table_init(2*MAX_J, 6);
	#pragma omp parallel
	wig_temp_init(2*MAX_J);
	was_wig_init = true;

}

void clear_wigxjpf() {

	wig_table_free();
	wig_temp_free();
	was_wig_init = false;

}

////////////////////////////////////////////////////////
////////////////////////////////////////////////////////
////////////////////////////////////////////////////////

#define W6U32 uint32_t

#define COLLECT_NEGATIVE(two_j1,two_j2,two_j3) do {     \
collect_sign |= (two_j1) | (two_j2) | (two_j3);     \
} while (0)

#define COLLECT_TRIANGLE_TRIVIAL_ZERO(two_j1,two_j2,two_j3) do {        \
collect_sign |= (((two_j2) + (two_j3)) - (two_j1));                 \
collect_sign |= ((two_j1) - ((two_j2) - (two_j3)));                 \
collect_sign |= ((two_j1) - ((two_j3) - (two_j2)));                 \
} while (0)

////////////////////////////////////////////////////////
////////////////////////////////////////////////////////
////////////////////////////////////////////////////////

void WIGNER6J_REGGE_CANONICALISE(char **ret, uint32_t two_j1, uint32_t two_j2, uint32_t two_j3,
                                             uint32_t two_j4, uint32_t two_j5, uint32_t two_j6){

  W6U32 b1 = (two_j1 + two_j2 + two_j3);
  W6U32 b2 = (two_j1 + two_j5 + two_j6);
  W6U32 b3 = (two_j4 + two_j2 + two_j6);
  W6U32 b4 = (two_j4 + two_j5 + two_j3);

  /* Check trivial-0 */
  W6U32 collect_sign = 0;
  W6U32 collect_odd = 0;

  collect_odd = b1 | b2 | b3 | b4;

  COLLECT_NEGATIVE(two_j1, two_j2, two_j3);
  COLLECT_NEGATIVE(two_j4, two_j5, two_j6);
  COLLECT_TRIANGLE_TRIVIAL_ZERO(two_j1, two_j2, two_j3);
  COLLECT_TRIANGLE_TRIVIAL_ZERO(two_j1, two_j5, two_j6);
  COLLECT_TRIANGLE_TRIVIAL_ZERO(two_j4, two_j2, two_j6);
  COLLECT_TRIANGLE_TRIVIAL_ZERO(two_j4, two_j5, two_j3);

  if ((collect_sign & (1 << (sizeof (int) * 8 - 1))) |
      (collect_odd & 1)){
    for (int i=0; i<=5; i++){
        (*ret)[i]=0;
    }
  }

  /* Check trivial-0 end */

  # define SHIFT_DOWN_1(a)  ((a) >> 1)

  b1 = SHIFT_DOWN_1(b1);
  b2 = SHIFT_DOWN_1(b2);
  b3 = SHIFT_DOWN_1(b3);
  b4 = SHIFT_DOWN_1(b4);

  W6U32 a1 = SHIFT_DOWN_1(two_j1 + two_j2 + two_j4 + two_j5);
  W6U32 a2 = SHIFT_DOWN_1(two_j1 + two_j3 + two_j4 + two_j6);
  W6U32 a3 = SHIFT_DOWN_1(two_j2 + two_j3 + two_j5 + two_j6);

  #define SWAP_TO_FIRST_LARGER(tmptype,a,b) do {		\
  tmptype __tmp_a = a;				\
  tmptype __tmp_b = b;				\
  a = (__tmp_a > __tmp_b) ? __tmp_a : __tmp_b;	\
  b = (__tmp_a < __tmp_b) ? __tmp_a : __tmp_b;	\
  } while (0)

  SWAP_TO_FIRST_LARGER(W6U32, a1, a2);
  SWAP_TO_FIRST_LARGER(W6U32, a2, a3);
  SWAP_TO_FIRST_LARGER(W6U32, a1, a2);

  SWAP_TO_FIRST_LARGER(W6U32, b1, b2);
  SWAP_TO_FIRST_LARGER(W6U32, b2, b3);
  SWAP_TO_FIRST_LARGER(W6U32, b3, b4);
  SWAP_TO_FIRST_LARGER(W6U32, b1, b2);
  SWAP_TO_FIRST_LARGER(W6U32, b2, b3);
  SWAP_TO_FIRST_LARGER(W6U32, b1, b2);

  // We now have a1 >= a2 >= a3, and b1 >= b2 >= b3 >= b4

  W6U32 S32 = a3 - b1;
  W6U32 B32 = a3 - b2;
  W6U32 T32 = a3 - b3;
  W6U32 X32 = a3 - b4;
  W6U32 L32 = a2 - b4;
  W6U32 E32 = a1 - b4;

  (*ret)[0]=E32;     (*ret)[1]=L32;     (*ret)[2]=X32;
  (*ret)[3]=T32;     (*ret)[4]=B32;     (*ret)[5]=S32;

}

#undef W6U32
#undef SWAP_TO_FIRST_LARGER

////////////////////////////////////////////////////////
////////////////////////////////////////////////////////
////////////////////////////////////////////////////////


void J6Symbol_Hash( kh_HashTableJ6_t *h, char **A,
                    unsigned int two_k1, unsigned int two_k3, unsigned int two_k2,
                    unsigned int two_l1, unsigned int two_l2, unsigned int two_l3){

	//////////////////// Convert spins to key////////////////////

  WIGNER6J_REGGE_CANONICALISE(A, two_k1, two_k3, two_k2, two_l1, two_l2, two_l3);

  HashTableJ6_key_t keyA = {(*A)[0],(*A)[1],(*A)[2],(*A)[3],(*A)[4],(*A)[5]};
	// Check we haven't already the function
	if (kh_get(HashTableJ6, h, keyA) == kh_end(h)){

		//////////////////// Save value////////////////////
	  int retA;
	  khint_t kA=kh_put(HashTableJ6, h, keyA, &retA);
	  if ( retA == 1 ){
	    double val6jA = wig6jj(two_k1,two_k3,two_k2,two_l1,two_l2,two_l3);
	    kh_val(h,kA) = val6jA;
	    kA=kh_get(HashTableJ6, h, keyA);
	    if( kh_val(h,kA) == 0 || kh_val(h,kA) != val6jA){
	        kh_val(h,kA) = val6jA;
	    }
	  }
	}
}

////////////////////////////////////////////////////////
////////////////////////////////////////////////////////
////////////////////////////////////////////////////////

void J9Symbol_Hash_Sum( kh_HashTableJ6_t *h, char **A,
                        unsigned int two_k2, unsigned int two_k3, unsigned int two_k1,
                        unsigned int two_j10, unsigned int two_j9, unsigned int two_i4,
                        unsigned int two_l7, unsigned int two_l8, unsigned int two_k5){

  unsigned int two_limx1 = max(max(abs(two_k2-two_k5),abs(two_j10-two_l8)),abs(two_k3-two_i4));
  unsigned int two_limx2 = min(min(two_k2+two_k5,two_j10+two_l8),two_k3+two_i4);

  ////////////////////////////
  //9j via 6j summation
  ////////////////////////////

  for( unsigned int two_x = two_limx1; two_x <= two_limx2; two_x+=2){

		//////////////////// Hash all 6j combinations for 9j ////////////////////
    J6Symbol_Hash ( h, A,
                    two_k2, two_j10, two_l7,
                    two_l8, two_k5, two_x);

    J6Symbol_Hash ( h, A,
                    two_k3, two_j9, two_l8,
                    two_j10, two_x, two_i4 );

    J6Symbol_Hash ( h, A,
                    two_k1, two_i4, two_k5,
                    two_x, two_k2, two_k3);
  }
}

////////////////////////////////////////////////////////
////////////////////////////////////////////////////////
////////////////////////////////////////////////////////

void J15Symbol_Hash( unsigned int two_j1, unsigned int two_j2, unsigned int two_j3,
                     unsigned int two_j4, unsigned int two_j5, unsigned int two_j6,
                     unsigned int two_j7, unsigned int two_j8, unsigned int two_j9,
                     unsigned int two_j10, unsigned int two_i4, unsigned int two_Dl ){

  unsigned int two_limi4_1, two_limi4_2;

  two_limi4_1 = max(abs(two_j5-two_j6),abs(two_j10-two_j9));
  two_limi4_2 = min(two_j5+two_j6,two_j10+two_j9);

  if(two_i4 < two_limi4_1 && two_i4 > two_limi4_2 )
  {
    printf("Boundary Intertwiner Problem");
    exit(EXIT_FAILURE);
  }

	////////Folder Check/////////
	struct stat st = {0};
	char  folderPath0[200], folderPath[200],  folderPath1[200],
				folderPath2[200], folderPath3[200];
	sprintf(folderPath0, "../data/");
	sprintf(folderPath, "../data/4simplex/");
	sprintf(folderPath1, "../data/4simplex/HashTablesJ6");
	sprintf(folderPath2, "../data/4simplex/aux/");
	sprintf(folderPath3, "../data/4simplex/aux/HashTablesKeyJ15/");

	if (stat(folderPath0, &st) == -1) {
    mkdir(folderPath0, 0700);
    mkdir(folderPath, 0700);
		mkdir(folderPath1, 0700);
		mkdir(folderPath2, 0700);
		mkdir(folderPath3, 0700);

	}
	if (stat(folderPath, &st) == -1) {
    mkdir(folderPath, 0700);
		mkdir(folderPath1, 0700);
		mkdir(folderPath2, 0700);
		mkdir(folderPath3, 0700);
	}
	if (stat(folderPath1, &st) == -1) {
		mkdir(folderPath1, 0700);
	}
	if (stat(folderPath2, &st) == -1) {
		mkdir(folderPath2, 0700);
		mkdir(folderPath3, 0700);
	}
	if (stat(folderPath3, &st) == -1) {
		mkdir(folderPath3, 0700);
	}
  char pathRead[200];
	char pathRead1[200];
	char pathRead2[200];
  sprintf(pathRead, "../data/4simplex/HashTablesJ6/%i.%i.%i.%i_%i_%i.6j",
          two_j5, two_j6, two_j9, two_j10, two_i4, two_Dl);
	sprintf(pathRead2, "../data/4simplex/aux/HashTablesKeyJ15/%i.%i.%i.%i_%i_%i.k15j",
          two_j5, two_j6, two_j9, two_j10, two_i4, two_Dl);

	//////////////////////////////////////////////////////////////////
  //////////////////// Hash Table initialization ///////////////////
  //////////////////////////////////////////////////////////////////

	//Check for previously computed tables.
  khash_t(HashTableJ6) *h = NULL;
	int data_yes = 0;
	int data_check = 0;
  for ( int i = two_Dl;  i >= 0; i-=2){
    sprintf(pathRead1, "../data/4simplex/HashTablesJ6/%i.%i.%i.%i_%i_%i.6j",
            two_j5, two_j6, two_j9, two_j10, two_i4, i);
    if(file_exist (pathRead1) != 0 ){
				//Data checks are used in case a Key hash table is present
				//but the actual data have been removed for some reason
				data_yes = 1;
				data_check = i;
        h = kh_load(HashTableJ6, pathRead1);
        break;
    }
  }
  if ( h == NULL){

      h = kh_init(HashTableJ6);
  }

	////////////////////  I initialize a table with keys to know ////////////////////
	////////////////////  what 15j pieces I've already computed ////////////////////

	khash_t(HashTableKeyJ15) *h2 = NULL;
	char pathRead3[200];
	for ( int i = two_Dl;  i >= 0; i-=2){
			sprintf(pathRead3, "../data/4simplex/aux/HashTablesKeyJ15/%i.%i.%i.%i_%i_%i.k15j",
							two_j5, two_j6, two_j9, two_j10, two_i4, i);
			if(file_exist (pathRead3) != 0 ){
					h2 = kh_load(HashTableKeyJ15, pathRead3);
					break;
			}
	}
	if ( h2 == NULL){
			h2 = kh_init(HashTableKeyJ15);
	}

	//////////////////// Check if I have the {6j}s. If not compute ////////////////////

	HashTableKeyJ15_key_t key = {two_j1,two_j2,two_j3,two_j4,two_j7,two_j8,two_Dl};

	if (data_yes == 1 && data_check == two_Dl &&
		 	kh_get(HashTableKeyJ15, h2, key) != kh_end(h2)){
			kh_destroy(HashTableKeyJ15, h2);
			kh_destroy(HashTableJ6, h);
			return ;
	}
	int ret;
	khint_t k = kh_put(HashTableKeyJ15, h2, key, &ret);

	//////////////////// Initialize pointer for J6 symbols ////////////////////

	char *A = malloc(6*sizeof(int));

	//////////////////// Start cycling all possible L combinations ////////////////////

	unsigned int two_l1, two_l2, two_l3, two_l4, two_l7, two_l8,
							 two_k2, two_k3, two_k5, two_k1;

  for(two_l1 = two_j1; two_l1 <= two_j1+two_Dl; two_l1+=2){
  for(two_l2 = two_j2; two_l2 <= two_j2+two_Dl; two_l2+=2){
  for(two_l3 = two_j3; two_l3 <= two_j3+two_Dl; two_l3+=2){
  for(two_l4 = two_j4; two_l4 <= two_j4+two_Dl; two_l4+=2) {
  for(two_l7 = two_j7; two_l7 <= two_j7+two_Dl; two_l7+=2){
  for(two_l8 = two_j8; two_l8 <= two_j8+two_Dl; two_l8+=2){

    unsigned int  two_limk2_1, two_limk2_2,two_limk3_1, two_limk3_2,
                  two_limk5_1, two_limk5_2;

    two_limk2_1 = max(abs(two_j10-two_l7),abs(two_l1-two_l2));
    two_limk2_2 = min(two_j10+two_l7,two_l1+two_l2);
    two_limk3_1 = max(abs(two_j9-two_l8),abs(two_l3-two_l1));
    two_limk3_2 = min(two_j9+two_l8,two_l3+two_l1);
    two_limk5_1 = max(abs(two_l4-two_j6),abs(two_l7-two_l8));
    two_limk5_2 = min(two_l4+two_j6,two_l7+two_l8);

    //////////////////// Start cycling all possible K combinations ////////////////////

    for(two_k2 = two_limk2_1; two_k2 <= two_limk2_2; two_k2+=2){
    for(two_k3 = two_limk3_1; two_k3 <= two_limk3_2; two_k3+=2){
    for(two_k5 = two_limk5_1; two_k5 <= two_limk5_2; two_k5+=2){
    for(two_k1 = max(max(abs(two_l3-two_l2),abs(two_j5-two_l4)),max(abs(two_k3-two_k2),abs(two_k5-two_i4)));
        two_k1 <= min(min(two_l3+two_l2,two_j5+two_l4),min(two_k2+two_k3,two_k5+two_i4)); two_k1+=2){

      ///////////////////////////////////////
      //6j Symbols - Key conversion and save
      ///////////////////////////////////////

      J6Symbol_Hash ( h, &A,
                      two_k1, two_k3, two_k2,
                      two_l1, two_l2, two_l3);
      J6Symbol_Hash ( h, &A,
                      two_k1, two_i4, two_k5,
                      two_j6, two_l4, two_j5);

      ///////////////////////////////////////////////////////
      //9j Symbols via 6j Summation - Key conversion and save
      ///////////////////////////////////////////////////////

      J9Symbol_Hash_Sum ( h, &A,
                          two_k2, two_k3, two_k1,
                          two_j10, two_j9, two_i4,
                          two_l7, two_l8, two_k5);
    }
    }
    }
    }
  }
  }
  }
  }
  }
	}
	free(A);
  //////////////////// Write the hash table to disk and free ////////////////////
	kh_write(HashTableKeyJ15, h2, pathRead2);
  kh_write(HashTableJ6, h, pathRead);
  kh_destroy(HashTableJ6, h);
	kh_destroy(HashTableKeyJ15, h2);

}

////////////////////////////////////////////////////////
////////////////////////////////////////////////////////
////////////////////////////////////////////////////////

double J15Symbol ( const kh_HashTableJ6_t *h, char **A,
            unsigned int two_l1, unsigned int two_l2, unsigned int two_l3, unsigned int two_l4, unsigned int two_j5,
            unsigned int two_j6, unsigned int two_l7, unsigned int two_l8, unsigned int two_j9, unsigned int two_j10,
            unsigned int two_i1, unsigned int two_i2, unsigned int two_i3, unsigned int two_i4, unsigned int two_i5,
            unsigned int two_k1, unsigned int two_k2, unsigned int two_k3, unsigned int two_k5){

  double CutOff = pow(10,-40);

  ////////////////////////////
  //Recover 6j and Compute 15j
  ////////////////////////////

  double val6jA,val6jB,val6jC,val6jD,val6jE;

  khiter_t kA,kB,kC,kD,kE;

  WIGNER6J_REGGE_CANONICALISE(A, two_k1, two_k3, two_k2, two_l1, two_l2, two_l3);
  HashTableJ6_key_t keyA = {(*A)[0],(*A)[1],(*A)[2],(*A)[3],(*A)[4],(*A)[5]};
  kA = kh_get(HashTableJ6, h, keyA);
  val6jA = kh_value(h, kA);
  if (fabs(val6jA) < CutOff) return 0;

  WIGNER6J_REGGE_CANONICALISE(A, two_k1, two_i4, two_k5, two_j6, two_l4, two_j5);
  HashTableJ6_key_t keyB = {(*A)[0],(*A)[1],(*A)[2],(*A)[3],(*A)[4],(*A)[5]};
  kB = kh_get(HashTableJ6, h, keyB);
  val6jB = kh_value(h, kB);
  if (fabs(val6jB) < CutOff) return 0;

  unsigned int two_limx1 = max(max(abs(two_k2-two_k5),abs(two_j10-two_l8)),abs(two_k3-two_i4));
  unsigned int two_limx2 = min(min(two_k2+two_k5,two_j10+two_l8),two_k3+two_i4);

  ////////////////////////////
  //9j via 6j summation
  ////////////////////////////

  double val9j = 0.0;

	#pragma omp parallel for reduction (+:val9j)
	for( unsigned int two_x = two_limx1; two_x <= two_limx2; two_x+=2){

    WIGNER6J_REGGE_CANONICALISE(A, two_k2,two_j10,two_l7,two_l8,two_k5,two_x);
    HashTableJ6_key_t keyC = {(*A)[0],(*A)[1],(*A)[2],(*A)[3],(*A)[4],(*A)[5]};
    kC = kh_get(HashTableJ6, h, keyC);
    val6jC = kh_value(h, kC);
    if (fabs(val6jC) < CutOff) continue;

    WIGNER6J_REGGE_CANONICALISE(A, two_k3,two_j9,two_l8,two_j10,two_x,two_i4);
    HashTableJ6_key_t keyD= {(*A)[0],(*A)[1],(*A)[2],(*A)[3],(*A)[4],(*A)[5]};
    kD = kh_get(HashTableJ6, h, keyD);
    val6jD = kh_value(h, kD);
    if (fabs(val6jD) < CutOff) continue;

    WIGNER6J_REGGE_CANONICALISE(A, two_k1,two_i4,two_k5,two_x,two_k2,two_k3);
    HashTableJ6_key_t keyE = {(*A)[0],(*A)[1],(*A)[2],(*A)[3],(*A)[4],(*A)[5]};
    kE = kh_get(HashTableJ6, h, keyE);
    val6jE = kh_value(h, kE);
    if (fabs(val6jE) < CutOff) continue;

    val9j += pow(-1,two_x)*(two_x+1.0)*val6jC*val6jD*val6jE;
 	}
  //printf ("%17g \n", val9j);
  double val15j = pow(-1,(two_l4+two_l3+two_k1)-(two_k2+two_k3+two_i4+two_k5)/2)
           *val6jA*val6jB*val9j;
  //printf ("%17g \n", val15j);
  return val15j;
}
