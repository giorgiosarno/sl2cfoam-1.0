/* Copyright 2018 Giorgio Sarno and Pietro Donà */

/* sl2cfoam is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   sl2cfoam is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.*/

#ifndef __B3FUNCTION_H__
#define __B3FUNCTION_H__
#include "common.h"
#include "b4function.h"

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
///Functions to compute B3 and SL2C Clebsch Gordan
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

#define EXIT_FAILURE_INEQUALITIES 0

//TODO At the present stage j_max = 50. We can not assure
//     that the result is correct for higher spins.
double complex X (unsigned int , unsigned int , unsigned int ,
                  unsigned int , unsigned int , unsigned int ,
                  float , float , float );
double complex Xsimp (unsigned int , unsigned int , unsigned int ,
                      unsigned int , unsigned int , unsigned int ,
                      float );
double B3 ( unsigned int , unsigned int , unsigned int ,
            unsigned int , unsigned int , unsigned int ,
            float );

// Kappa function has been written by Francesco
// Gorrini using MPC and arbitrary precision
// gamma function
static void kappa (mpc_t , int ,  int ,  int ,
                          int ,  int ,  int ,
											    double , double , double) ;


#endif/*__B3FUNCTION_H__*/
