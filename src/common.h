/* Copyright 2018 Giorgio Sarno and Pietro Donà */

/* sl2cfoam is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   sl2cfoam is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.*/

#ifndef __COMMON_H__
#define __COMMON_H__

///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
///Common inc and functions for the library
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <complex.h>
#include <string.h>
#include <assert.h>
#include <stdbool.h>
#include <tgmath.h>
#include <gmp.h>                  //Precision Library
#include <mpfr.h>                 //Precision Library
#include <mpc.h>                  //Precision Library
#include <omp.h>                  // Parallelization utilities
#include <unistd.h>               // To check if a file already exists
#include <sys/stat.h>             // To check if a file already exists
#include <gsl/gsl_sf_gamma.h>           //GSL complex gamma function
#include <gsl/gsl_sf_result.h>

#include "khash.h"                //Hash Table
#include "wigxjpf.h"              //To compute 3j,6j symbols
#include "utilities.h"            //various utilities
#include "config.h"               //configuration macros
#include "cgamma.h"

#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define d(two_a) (two_a+1)

#define m_pi 3.14159265358979323846

///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
///Common definition of Hash tables
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////

// 24 bytes keys (6 int * 4 bytes) with double values for 6j symbols
ARRAY_TABLE_INIT(HashTableJ6, 6*sizeof(unsigned int), double);
// 40 bytes keys with double values for boosters
ARRAY_TABLE_INIT(HashTableBooster, 10*sizeof(unsigned int), double);
// 24 bytes keys for auxiliary 15j pieces keys
ARRAY_TABLE_INIT(HashTableKeyJ15, 6*sizeof(unsigned int), int);
// 28 bytes keys for auxiliary booster keys
ARRAY_TABLE_INIT(HashTableKeyBooster, 7*sizeof(unsigned int), int);
// 40 bytes keys with double values for eprl 4-simplices
ARRAY_TABLE_INIT(HashTableEPRL, 10*sizeof(unsigned int), double);
// 28 bytes keys with double values for 3-booster keys
ARRAY_TABLE_INIT(HashTableB3, 7*sizeof(unsigned int), double);
// 12 bytes keys with double values for eprl 3-simplices
ARRAY_TABLE_INIT(HashTableEPRL3, 3*sizeof(unsigned int), double);

#endif/*__COMMON_H__*/
