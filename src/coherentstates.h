/* Copyright 2018 Giorgio Sarno and Pietro Donà*/

/* sl2cfoam is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   sl2cfoam is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.*/


#ifndef __COHERENTSTATES_H__
#define __COHERENTSTATES_H__

#include "common.h"

//////////////////// Coherent Tetrahedron at intertwiner I ////////////////////

//This function requieres four addition int as input: they have to be +1 or -1 depending on
//the strand's orientation. An "ingoing" strand going from the boundary node
//to the intertwiner requires a -1 while an "outgoing" strand, from the intertwiner
// to the boundary node requires a +1.

double complex CoherentStateI(unsigned int, unsigned int, unsigned int, unsigned int, unsigned int,
                              double, double, double, double, double, double, double, double,
                              int, int, int, int);

//////////////////// SU(2) Wigner D Matrix ////////////////////

double complex WignerD (unsigned int ,int ,double ,double, int );


#endif /*__COHERENTSTATES_H__*/
