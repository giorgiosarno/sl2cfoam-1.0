/* Copyright 2018 Giorgio Sarno and Pietro Donà */

/* sl2cfoam is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   sl2cfoam is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.*/


#include "utilities.h"

////////////////////////////////////////////////////////
////////////////////////////////////////////////////////
////////////////////////////////////////////////////////

int file_exist (char *filename){

    struct stat   buffer;
    return (stat (filename, &buffer) == 0);
}

double complex MPFRComplexMultiplication(double complex coherentA, double complex coherentB){

  mpfr_t  statesMPFR_A_Re, statesMPFR_B_Re, statesMPFR_A_Im, statesMPFR_B_Im,
          statesMPFR_Re1,statesMPFR_Re2, statesMPFR_Im1,statesMPFR_Im2,
          statesMPFR1, statesMPFR2;

  mpfr_rnd_t GMP_RNDN;

  mpfr_init_set_d (statesMPFR_A_Re, creal(coherentA), GMP_RNDN);
  mpfr_init_set_d (statesMPFR_B_Re, creal(coherentB), GMP_RNDN);
  mpfr_init_set_d (statesMPFR_A_Im, cimag(coherentA), GMP_RNDN);
  mpfr_init_set_d (statesMPFR_B_Im, cimag(coherentB), GMP_RNDN);

  mpfr_inits (statesMPFR_Re1, statesMPFR_Re2, statesMPFR_Im1, statesMPFR_Im2, NULL);

  mpfr_mul (statesMPFR_Re1, statesMPFR_A_Re, statesMPFR_B_Re, GMP_RNDN);
  mpfr_mul (statesMPFR_Re2, statesMPFR_A_Im, statesMPFR_B_Im, GMP_RNDN);
  mpfr_mul (statesMPFR_Im1, statesMPFR_A_Im, statesMPFR_B_Re, GMP_RNDN);
  mpfr_mul (statesMPFR_Im2, statesMPFR_A_Re, statesMPFR_B_Im, GMP_RNDN);

  mpfr_inits (statesMPFR1,statesMPFR2, NULL);
  mpfr_sub  (statesMPFR1, statesMPFR_Re1, statesMPFR_Re2, GMP_RNDN);
  mpfr_add  (statesMPFR2, statesMPFR_Im1, statesMPFR_Im2, GMP_RNDN);

  double complex result = mpfr_get_d (statesMPFR1, GMP_RNDN) + I*mpfr_get_d (statesMPFR2, GMP_RNDN);

  mpfr_clears ( statesMPFR_A_Re, statesMPFR_B_Re, statesMPFR_A_Im, statesMPFR_B_Im,
                statesMPFR_Re1,statesMPFR_Re2, statesMPFR_Im1,statesMPFR_Im2,
                statesMPFR1, statesMPFR2, NULL );

  return result;
}

double complex MPFRComplexDivision(double complex coherentA, double complex coherentB){
  //coherentA / coherentB
  mpfr_t  statesMPFR_A_Re, statesMPFR_B_Re, statesMPFR_A_Im, statesMPFR_B_Im,
          statesMPFR_Den1,statesMPFR_Den2,
          statesMPFR_Re1,statesMPFR_Re2, statesMPFR_Im1,statesMPFR_Im2,
          statesMPFR1, statesMPFR2;

  mpfr_rnd_t GMP_RNDN;

  mpfr_init_set_d (statesMPFR_A_Re, creal(coherentA), GMP_RNDN);
  mpfr_init_set_d (statesMPFR_A_Im, cimag(coherentA), GMP_RNDN);

  mpfr_init_set_d (statesMPFR_B_Re, creal(coherentB), GMP_RNDN);
  mpfr_init_set_d (statesMPFR_B_Im, cimag(coherentB), GMP_RNDN);

  mpfr_inits (statesMPFR_Re1, statesMPFR_Re2, statesMPFR_Im1, statesMPFR_Im2,
              statesMPFR_Den1,statesMPFR_Den2,NULL);

  mpfr_mul (statesMPFR_Re1, statesMPFR_A_Re, statesMPFR_B_Re, GMP_RNDN);
  mpfr_mul (statesMPFR_Re2, statesMPFR_A_Im, statesMPFR_B_Im, GMP_RNDN);

  mpfr_mul (statesMPFR_Im1, statesMPFR_A_Im, statesMPFR_B_Re, GMP_RNDN);
  mpfr_mul (statesMPFR_Im2, statesMPFR_A_Re, statesMPFR_B_Im, GMP_RNDN);

  mpfr_pow_ui(statesMPFR_Den1, statesMPFR_B_Re, 2, GMP_RNDN);
  mpfr_pow_ui(statesMPFR_Den2, statesMPFR_B_Im, 2, GMP_RNDN);
  mpfr_add (statesMPFR_Den1, statesMPFR_Den1, statesMPFR_Den2, GMP_RNDN );

  mpfr_div (statesMPFR_Re1, statesMPFR_Re1, statesMPFR_Den1, GMP_RNDN);
  mpfr_div (statesMPFR_Re2, statesMPFR_Re2, statesMPFR_Den1, GMP_RNDN);
  mpfr_div (statesMPFR_Im1, statesMPFR_Im1, statesMPFR_Den1, GMP_RNDN);
  mpfr_div (statesMPFR_Im2, statesMPFR_Im2, statesMPFR_Den1, GMP_RNDN);

  mpfr_inits (statesMPFR1,statesMPFR2, NULL);
  mpfr_add  (statesMPFR1, statesMPFR_Re1, statesMPFR_Re2, GMP_RNDN);
  mpfr_sub  (statesMPFR2, statesMPFR_Im1, statesMPFR_Im2, GMP_RNDN);

  double complex result = mpfr_get_d (statesMPFR1, GMP_RNDN) + I*mpfr_get_d (statesMPFR2, GMP_RNDN);

  mpfr_clears ( statesMPFR_A_Re, statesMPFR_B_Re, statesMPFR_A_Im, statesMPFR_B_Im,
                statesMPFR_Re1,statesMPFR_Re2, statesMPFR_Im1,statesMPFR_Im2,
                statesMPFR1, statesMPFR2, NULL );

  return result;
}

void CompensatedSummationMPFR_EPRL (double *err, double *value,
                                    mpfr_t SU2ValueMPFR ){

  mpfr_rnd_t GMP_RNDN;

  double Aux1,Aux2,Aux3;
  Aux1 = mpfr_get_d (SU2ValueMPFR, GMP_RNDN) ;
  Aux2 = *value + Aux1;
  Aux3 = (Aux2 - *value) - Aux1;
  *err = Aux3;
  *value =  Aux2;

}

void CompensatedSummationMPFR_Complex ( double complex *err, double complex *SU2Value,
                                        mpfr_t SU2ValueMPFR_Re, mpfr_t SU2ValueMPFR_Im ){

  mpfr_rnd_t GMP_RNDN;

  double complex Aux1,Aux2,Aux3;

  Aux1 = mpfr_get_d (SU2ValueMPFR_Re, GMP_RNDN) + mpfr_get_d (SU2ValueMPFR_Im, GMP_RNDN)*I;
  Aux2 = *SU2Value + Aux1;
  Aux3 = (Aux2 - *SU2Value) - Aux1;
  *err = Aux3;
  *SU2Value =  Aux2;

}


void Data_Folder_4 (float Immirzi){

  float ImmirziSave = trunc(100 * Immirzi) / 100;
  struct stat st = {0};

  if (stat("../data/", &st) == -1) {
      mkdir("../data/", 0700);
  }

  char  folderPath[200],  folderPath1[200], folderPath2[200], folderPath3[200],
        folderPath5[200], folderPath6[200], folderPath7[200], folderPath8[200],
        folderPath9[200];

  sprintf(folderPath, "../data/4simplex/");
  sprintf(folderPath1, "../data/4simplex/HashTablesJ6");
  sprintf(folderPath2, "../data/4simplex/Immirzi_%.2f",ImmirziSave);
  sprintf(folderPath3, "../data/4simplex/Immirzi_%.2f/HashTablesBooster",ImmirziSave);
  sprintf(folderPath5, "../data/4simplex/Immirzi_%.2f/HashTablesEPRL",ImmirziSave);
  sprintf(folderPath6, "../data/4simplex/aux");
  sprintf(folderPath7, "..//data/4simplex/aux/Immirzi_%.2f",ImmirziSave);
  sprintf(folderPath8, "../data/4simplex/aux/HashTablesKeyJ15");
  sprintf(folderPath9, "../data/4simplex/aux/Immirzi_%.2f/HashTablesKeyBooster",ImmirziSave);
  if (stat(folderPath, &st) == -1) {

    mkdir(folderPath, 0700);
    mkdir(folderPath1, 0700);
    mkdir(folderPath2, 0700);
    mkdir(folderPath3, 0700);
    mkdir(folderPath5, 0700);
  }
  if (stat(folderPath6, &st) == -1) {
    mkdir(folderPath6, 0700);
    mkdir(folderPath7, 0700);
    mkdir(folderPath8, 0700);
    mkdir(folderPath9, 0700);
  }
  if (stat(folderPath7, &st) == -1) {
    mkdir(folderPath7, 0700);
    mkdir(folderPath9, 0700);
  }
  if (stat(folderPath8, &st) == -1) {
    mkdir(folderPath8, 0700);
  }
  if (stat(folderPath9, &st) == -1) {
    mkdir(folderPath9, 0700);
  }
  if (stat(folderPath1, &st) == -1) {
    mkdir(folderPath1, 0700);
  }
  if (stat(folderPath2, &st) == -1) {
    mkdir(folderPath2, 0700);
    mkdir(folderPath3, 0700);
    mkdir(folderPath5, 0700);
  }
  if (stat(folderPath3, &st) == -1) {
      mkdir(folderPath3, 0700);
  }
  if (stat(folderPath5, &st) == -1) {
      mkdir(folderPath5, 0700);
  }
}

void Data_Folder_3 (float Immirzi){

  float ImmirziSave = trunc(100 * Immirzi) / 100;
  struct stat st = {0};

  if (stat("../data/", &st) == -1) {
      mkdir("../data/", 0700);
  }

  char  folderPath[200], folderPath1[200], folderPath2[200], folderPath3[200],
        folderPath5[200], folderPath6[200], folderPath7[200], folderPath8[200],
        folderPath9[200];

  sprintf(folderPath, "../data/3simplex/");
  sprintf(folderPath1, "../data/3simplex/HashTablesJ6");
  sprintf(folderPath2, "../data/3simplex/Immirzi_%.2f",ImmirziSave);
  sprintf(folderPath3, "../data/3simplex/Immirzi_%.2f/HashTablesBooster",ImmirziSave);
  sprintf(folderPath5, "../data/3simplex/Immirzi_%.2f/HashTablesEPRL",ImmirziSave);

  if (stat(folderPath, &st) == -1) {

    mkdir(folderPath, 0700);
    mkdir(folderPath1, 0700);
    mkdir(folderPath2, 0700);
    mkdir(folderPath3, 0700);
    mkdir(folderPath5, 0700);

  }

  if (stat(folderPath1, &st) == -1) {
    mkdir(folderPath1, 0700);
  }
  if (stat(folderPath2, &st) == -1) {
    mkdir(folderPath2, 0700);
    mkdir(folderPath3, 0700);
    mkdir(folderPath5, 0700);
  }
  if (stat(folderPath3, &st) == -1) {
      mkdir(folderPath3, 0700);
  }
  if (stat(folderPath5, &st) == -1) {
      mkdir(folderPath5, 0700);
  }
}

//////////////////// (-1)^a real and (-1)^b complex////////////////////

double complex complex_negpow(int two_j) {

	int k = two_j % 2; // i factor
	int j = two_j / 2;

	if (k == 1) {
		if (j % 2 == 0) {
	    	return I;
		}
		return -I;
	}

	if (k == -1) {
		if (j % 2 == 0) {
	    	return -I;
		}
		return I;
	}

	if (j % 2 == 0) {
	    return 1;
	}
	return -1;

}

int real_negpow(int tj) {

	// ensure integer argument
  assert((tj % 2) == 0);

	int j = tj / 2;

	if (j % 2 == 0) {
	    return 1;
	}
	return -1;

}
