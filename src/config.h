/* Copyright 2018 Giorgio Sarno and Pietro Donà */

/* sl2cfoam is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   sl2cfoam is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.*/


#ifndef __CONFIG_H__
#define __CONFIG_H__

////////////////////////////////////////////////////////////////////////
// Global parameters for the amplitude
////////////////////////////////////////////////////////////////////////

// To enable data saving
#define SAVE_DATA 1

// To enable data printing on shell
#define PRINT_DATA 0

// High precision=1 sets N=10000 as integration points for B4
// Otherwise N=3000, it has been tested up to 5/6 l's shell and
// it works well.
// TODO Other tests and possible dynamical way of setting
//      the integration precision
#define HIGH_PRECISION 0
// Low precision=1 sets N=1000 as integration points for B4
#define LOW_PRECISION 0


// Set bits precision for MPFR/MPC
// B3 functions require 2*128 bits
#define PRECISION 2*128

#endif/*__CONFIG_H__*/
