/* Copyright 2018 Giorgio Sarno and Pietro Donà */

/* sl2cfoam is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   sl2cfoam is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.*/


#ifndef __UTILITIES_H__
#define __UTILITIES_H__

#include "common.h"

///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
///Various utilities for computing and storing the amplitude
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////

//////////////////// Multiplication of complex values as MPFR variables ////////////////////

double complex MPFRComplexMultiplication(double complex , double complex );

//////////////////// Division of complex values as MPFR variables ////////////////////

double complex MPFRComplexDivision(double complex , double complex );

//////////////////// File Check ////////////////////

int file_exist ( char * );

//////////////////// Data Folder Check for 4-Simplex ////////////////////

void Data_Folder_4 (float );

//////////////////// Data Folder Check for 3-Simplex ////////////////////

void Data_Folder_3 (float );

//////////////////// Fail Printer ////////////////////

#define fail(str) {\
            fprintf(stderr, str);\
            fprintf(stderr, "\n");\
            exit(EXIT_FAILURE);\
        }

//////////////////// Compensated Summation Alghoritm ////////////////////

void CompensatedSummationMPFR_EPRL (double *, double *, mpfr_t );
void CompensatedSummationMPFR_Complex (double complex *, double complex *, mpfr_t , mpfr_t );

//////////////////// (-1)^a real and (-1)^b complex////////////////////

double complex complex_negpow(int two_j) ;
int real_negpow(int tj);

#endif /*__UTILITIES_H__*/
