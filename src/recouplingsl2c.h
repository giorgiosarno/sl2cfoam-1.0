/* Copyright 2018 Giorgio Sarno and Pietro Donà */

/* sl2cfoam is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   sl2cfoam is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.*/

#ifndef __RECOUPLINGSL2C_H__
#define __RECOUPLINGSL2C_H__

#include "common.h"
#include "b3function.h"

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
///Functions to compute SL(2,C) recoupling theory 3j and 6j
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

double  wigner_3j_sl2c ( unsigned int , unsigned int , unsigned int ,
                         unsigned int , unsigned int , unsigned int ,
                         float , float , float );

double  wigner_6j_sl2c ( unsigned int , unsigned int , unsigned int ,
                         unsigned int , unsigned int , unsigned int ,
                         float , float , float ,
                         float , float , float ,
                         unsigned int );

////////////////////////////////////////////////////////
////////////////////////////////////////////////////////
////////////////////////////////////////////////////////

#endif/*__RECOUPLINGSL2C_H__*/
