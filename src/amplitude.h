/* Copyright 2018 Giorgio Sarno and Pietro Donà */

/* sl2cfoam is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   sl2cfoam is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.*/

#ifndef __AMPLITUDE_H__
#define __AMPLITUDE_H__

#include "common.h"
#include "jsymbols.h"
#include "b4function.h"
#include "b3function.h"
#include "recouplingsl2c.h"
#include "coherentstates.h"

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
///Functions to Compute EPRL 4-simplices
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

double FourSimplex (unsigned int , unsigned int , unsigned int , unsigned int ,
                    unsigned int , unsigned int , unsigned int , unsigned int ,
                    unsigned int , unsigned int ,
                    unsigned int , unsigned int , unsigned int ,
                    unsigned int , unsigned int ,
                    unsigned int , float );

////////////////////////////////////////////////////////
///Functions to Compute EPRL 3-simplices
////////////////////////////////////////////////////////

//TODO At the present stage j_max = 50. We can not assure
//     that the result is correct for higher spins.

double ThreeSimplex ( unsigned int , unsigned int , unsigned int ,
                      unsigned int , unsigned int , unsigned int ,
                      unsigned int , float );

#endif /*__AMPLITUDE_H__*/
