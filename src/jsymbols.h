/* Copyright 2018 Giorgio Sarno and Pietro Donà */

/* sl2cfoam is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   sl2cfoam is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.*/

/*sl2cfoam uses and modify WIGXJPF, Copyright 2015 Haakan T. Johansson
 	All details can be found at https://arxiv.org/abs/1504.08329*/


#ifndef __J6CANONICALIZATION_H__
#define __J6CANONICALIZATION__

#include "common.h"

typedef __uint32_t  uint32_t;

///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
///Initialization utilities for wigxjpf 3j,6j library
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////

void init_wigxjpf() ;
void clear_wigxjpf();

///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
///Various function to compute and store 6j,9j,15j symbols
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////

//////////////////// Canonicalization function for 6j symbols ////////////////////

void WIGNER6J_REGGE_CANONICALISE( char **ret, uint32_t two_j1, uint32_t two_j2, uint32_t two_j3,
                                              uint32_t two_j4, uint32_t two_j5, uint32_t two_j6
                                );

//////////////////// 6J symbol Hash Function ////////////////////

void J6Symbol_Hash( kh_HashTableJ6_t *, char **,
                    unsigned int , unsigned int , unsigned int ,
                    unsigned int , unsigned int , unsigned int
                  );

//////////////////// 9J symbol Hash Function via 6j summation ////////////////////

void J9Symbol_Hash_Sum( kh_HashTableJ6_t *, char **,
                        unsigned int , unsigned int , unsigned int ,
                        unsigned int , unsigned int , unsigned int ,
                        unsigned int , unsigned int , unsigned int
                      );

//////////////////// 15J symbol Hash Function ////////////////////

void J15Symbol_Hash( unsigned int , unsigned int , unsigned int ,
                     unsigned int , unsigned int , unsigned int ,
                     unsigned int , unsigned int , unsigned int ,
                     unsigned int , unsigned int , unsigned int
                    );

//////////////////// 15J symbol Function ////////////////////

double J15Symbol( const kh_HashTableJ6_t *, char **,
                 unsigned int , unsigned int , unsigned int , unsigned int , unsigned int ,
                 unsigned int , unsigned int , unsigned int , unsigned int , unsigned int ,
                 unsigned int , unsigned int , unsigned int , unsigned int , unsigned int ,
                 unsigned int , unsigned int , unsigned int , unsigned int
               );

#endif /*__J6CANONICALIZATION_H__*/
