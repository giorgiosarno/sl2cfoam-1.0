/* Copyright 2018 Giorgio Sarno, Pietro Donà and Francesco Gozzini */

/* sl2cfoam is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   sl2cfoam is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.*/

#include "b3function.h"

////////////////////////////////////////////////////////
////////////////////////////////////////////////////////
////////////////////////////////////////////////////////


static void kappa(	mpc_t rop, int two_j1,  int two_j2,  int two_j3,
                    int two_k1,  int two_k2,  int two_k3,
										double two_rho1, double two_rho2, double two_rho3) {

  // check for integers
  assert((two_j1+two_j2-two_j3) % 2 == 0);
  assert((two_j2+two_j3-two_j1) % 2 == 0);
  assert((two_j1+two_j3-two_j2) % 2 == 0);
  assert((two_j1+two_j2+two_j3) % 2 == 0);

	double j1 = ((double)two_j1)/2.0;
 	double j2 = ((double)two_j2)/2.0;
 	double j3 = ((double)two_j3)/2.0;

 	double k1 = ((double)two_k1)/2.0;
 	double k2 = ((double)two_k2)/2.0;
 	double k3 = ((double)two_k3)/2.0;
 	double K = k1+k2+k3;

 	double rho_1 = two_rho1/2.0;
 	double rho_2 = two_rho2/2.0;
 	double rho_3 = two_rho3/2.0;

  mpc_t result;
  mpc_init2(result, PRECISION);

  // real ap vars
  mpfr_t x, y;
  mpfr_init2(x, PRECISION);
  mpfr_init2(y, PRECISION);

  // complex ap vars
  mpc_t z, w;
  mpc_init2(z, PRECISION);
  mpc_init2(w, PRECISION);

  // first factors in the product, before the sums
	double complex phase = complex_negpow(-two_k1-two_k2 + two_j1-two_j2+two_j3);
  mpc_set_dc(result, phase, MPC_RNDNN);

  mpfr_set_ui(x, (unsigned long int)(d(two_j3)), MPFR_RNDN);
  mpfr_sqrt(y, x, MPFR_RNDN);
  mpc_div_fr(result, result, y, MPC_RNDNN);

  // phase part
  mpc_set_d_d(z, 1.0 + j3, rho_3, MPC_RNDNN);
  complex_lngamma(w, z);

  mpc_set_d_d(z, 1.0 + j1, -rho_1, MPC_RNDNN);
  complex_lngamma(z, z);
  mpc_add(w, w, z, MPC_RNDNN);

  mpc_set_d_d(z, 1.0 + j2, -rho_2, MPC_RNDNN);
  complex_lngamma(z, z);
  mpc_add(w, w, z, MPC_RNDNN);

  mpc_exp(w, w, MPC_RNDNN);

  mpc_abs(x, w, MPC_RNDNN);
  mpc_div_fr(w, w, x, MPC_RNDNN);
	//printf ("G %i %i %i %17g %17g \n",two_j1,two_j2,two_j3,mpc_get_dc(w,MPC_RNDNN));
  mpc_mul(result, result, w, MPC_RNDNN);

  // check the factorial are non-negative
  assert((two_j1 - two_k1) >= 0);
  assert((two_j2 + two_k2) >= 0);
  assert((two_j1 + two_k1) >= 0);
  assert((two_j2 - two_k2) >= 0);

  // sqrt of factorial before the sums
  mpfr_fac_ui(x, (unsigned int)(two_j1 - two_k1)/2, MPFR_RNDN);
  mpfr_fac_ui(y, (unsigned int)(two_j2 + two_k2)/2, MPFR_RNDN);
  mpfr_mul(x, x, y, MPFR_RNDN);
  mpfr_fac_ui(y, (unsigned int)(two_j1 + two_k1)/2, MPFR_RNDN);
  mpfr_div(x, x, y, MPFR_RNDN);
  mpfr_fac_ui(y, (unsigned int)(two_j2 - two_k2)/2, MPFR_RNDN);
  mpfr_div(x, x, y, MPFR_RNDN);
  mpfr_sqrt(x, x, MPFR_RNDN);
  mpc_mul_fr(result, result, x, MPC_RNDNN);

  // now the sums remain ...
  mpc_t nsum, ssum, s1prod, s2prod;
  mpc_init2(nsum, PRECISION);
  mpc_init2(ssum, PRECISION);
  mpc_init2(s1prod, PRECISION);
  mpc_init2(s2prod, PRECISION);
  mpc_set_ui(nsum, 0, MPC_RNDNN);

  mpfr_t spref;
  mpfr_init2(spref, PRECISION);

  double wig;
  int two_n, two_s1, two_s2;
  float s1, s2;

  for (two_n = -two_j1; two_n <= min(two_j1, two_k3 + two_j2); two_n += 2) {

    // check for allowed summands
    if (two_j1-two_n < 0) {
        continue;
    }
    if (two_j2+two_k3-two_n < 0) {
        continue;
    }
    if (two_j1+two_n < 0) {
        continue;
    }
    if (two_j2-two_k3+two_n < 0) {
        continue;
    }

    // sqrt of factorial into sum over n

    mpfr_fac_ui(x, (unsigned int)(two_j1 + two_n)/2, MPFR_RNDN);
    mpfr_fac_ui(y, (unsigned int)(two_j2 - two_k3 + two_n)/2, MPFR_RNDN);
    mpfr_mul(x, x, y, MPFR_RNDN);

    mpfr_fac_ui(y, (unsigned long int)(two_j1 - two_n)/2, MPFR_RNDN);
    mpfr_div(x, y, x, MPFR_RNDN);
    mpfr_fac_ui(y, (unsigned int)(two_j2 + two_k3 - two_n)/2, MPFR_RNDN);
    mpfr_mul(x, x, y, MPFR_RNDN);

    mpfr_sqrt(spref, x, MPFR_RNDN);

    wig = wig3jj(	two_j1, two_j2, two_j3,
        					two_n,  two_k3-two_n, -two_k3);

    mpfr_mul_d(spref, spref, wig, MPFR_RNDN);
    mpc_set_ui(ssum, 0, MPC_RNDNN);

    for (two_s1 = max(two_k1, two_n); two_s1 <= two_j1; two_s1 += 2) {

      s1 = ((double)two_s1) / 2.0;

      // check for allowed summands
      if (two_j1+two_s1 < 0) {
          continue;
      }
      if (two_j1-two_s1 < 0) {
          continue;
      }
      if (two_s1-two_k1 < 0) {
          continue;
      }
      if (two_s1-two_n < 0) {
          continue;
      }

      mpfr_fac_ui(x, (unsigned int)(two_j1-two_s1)/2, MPFR_RNDN);
      mpfr_fac_ui(y, (unsigned int)(two_s1-two_k1)/2, MPFR_RNDN);
      mpfr_mul(x, x, y, MPFR_RNDN);
      mpfr_fac_ui(y, (unsigned int)(two_s1-two_n)/2, MPFR_RNDN);
      mpfr_mul(x, x, y, MPFR_RNDN);

      mpfr_fac_ui(y, (unsigned int)(two_j1+two_s1)/2, MPFR_RNDN);
      mpfr_div(x, y, x, MPFR_RNDN);

      mpc_set_fr(s1prod, x, MPC_RNDNN);

      mpc_set_d_d(z, (1.0-K+two_s1)/2.0, -(rho_1-rho_2-rho_3)/2.0, MPC_RNDNN);
      complex_lngamma(w, z);

      mpc_set_d_d(z, 1.0+s1, -rho_1, MPC_RNDNN);
      complex_lngamma(z, z);
      mpc_sub(w, w, z, MPC_RNDNN);

      mpc_exp(w, w, MPC_RNDNN);
      mpc_mul(s1prod, s1prod, w, MPC_RNDNN);

      for (two_s2 = max(-two_k2, two_n-two_k3); two_s2 <= two_j2; two_s2 += 2) {

        s2 = ((double)two_s2) / 2.0;

        // check for allowed summands
        if (two_j2+two_s2 < 0) {
            continue;
        }
        if (two_j2-two_s2 < 0) {
            continue;
        }
        if (two_k2+two_s2 < 0) {
            continue;
        }
        if (two_k3-two_n+two_s2 < 0) {
            continue;
        }

        mpc_set(s2prod, s1prod, MPC_RNDNN);
				double complex phase_s = complex_negpow(two_s1+two_s2-two_k1+two_k2);
        mpc_set_dc(z, phase_s , MPC_RNDNN);
        mpc_mul(s2prod, s2prod, z, MPC_RNDNN);

        mpfr_fac_ui(x, (unsigned int)(two_j2-two_s2)/2, MPFR_RNDN);
        mpfr_fac_ui(y, (unsigned int)(two_s2+two_k2)/2, MPFR_RNDN);
        mpfr_mul(x, x, y, MPFR_RNDN);
        mpfr_fac_ui(y, (unsigned int)(two_k3-two_n+two_s2)/2, MPFR_RNDN);
        mpfr_mul(x, x, y, MPFR_RNDN);

        mpfr_fac_ui(y, (unsigned int)(two_j2+two_s2)/2, MPFR_RNDN);
        mpfr_div(x, y, x, MPFR_RNDN);

        mpc_mul_fr(s2prod, s2prod, x, MPC_RNDNN);

        mpc_set_d_d(z, (1.0+K+two_s2)/2.0, +(rho_1-rho_2+rho_3)/2.0, MPC_RNDNN);
        complex_lngamma(w, z);

        mpc_set_d_d(z, (1.0-k1+k2+k3-two_n+two_s1+two_s2)/2.0, -(rho_1+rho_2-rho_3)/2.0, MPC_RNDNN);
        complex_lngamma(z, z);
        mpc_add(w, w, z, MPC_RNDNN);

        mpc_set_d_d(z, 1.0+s2, -rho_2, MPC_RNDNN);
        complex_lngamma(z, z);
        mpc_sub(w, w, z, MPC_RNDNN);

        mpc_set_d_d(z, 1.0+s1+s2, rho_3, MPC_RNDNN);
        complex_lngamma(z, z);
        mpc_sub(w, w, z, MPC_RNDNN);

        mpc_exp(w, w, MPC_RNDNN);
        mpc_mul(s2prod, s2prod, w, MPC_RNDNN);

        mpc_add(ssum, ssum, s2prod, MPC_RNDNN);
      }
    }

    mpc_set_d_d(z, (1.0-k1+k2+k3-two_n)/2.0, -(rho_1+rho_2+rho_3)/2.0, MPC_RNDNN);
    complex_lngamma(w, z);
    mpc_exp(w, w, MPC_RNDNN);
    mpc_div(ssum, ssum, w, MPC_RNDNN);

    mpc_mul_fr(ssum, ssum, spref, MPC_RNDNN);
    mpc_add(nsum, nsum, ssum, MPC_RNDNN);

  }

  mpc_mul(result, result, nsum, MPC_RNDNN);
	mpc_set(rop, result, MPC_RNDNN);
  // clear variables
  mpfr_clear(x);
  mpfr_clear(y);
  mpfr_clear(spref);
  mpc_clear(result);
  mpc_clear(z);
  mpc_clear(w);
  mpc_clear(nsum);
  mpc_clear(ssum);
  mpc_clear(s1prod);
  mpc_clear(s2prod);

}

double complex X (unsigned int two_j1, unsigned int two_j2, unsigned int two_j3,
                  unsigned int two_k1, unsigned int two_k2, unsigned int two_k3,
                  float two_rho1, float two_rho2, float two_rho3){

  float j1, j2, j3, k1, k2, k3, rho1, rho2, rho3;

  j1 = (float)(two_j1)/2.0; j2 = (float)(two_j2)/2.0; j3 = (float)(two_j3)/2.0;
  k1 = (float)(two_k1)/2.0; k2 = (float)(two_k2)/2.0; k3 = (float)(two_k3)/2.0;
  rho1 = two_rho1/2.0; rho2 = two_rho2/2.0; rho3 = two_rho3/2.0;

  double complex phase = complex_negpow ((int)((float)((two_j1+two_j2+two_j3+two_k1+two_k2+two_k3)/2.0)))/(4*sqrt(2*m_pi));

	mpfr_t x;
	mpfr_init2(x, PRECISION);

	mpc_t z,w,y;
	mpc_init2(z, PRECISION);
	mpc_init2(w, PRECISION);
	mpc_init2(y, PRECISION);

	mpc_set_d_d(z, (1-k1-k2-k3)/2, (rho1+rho2+rho3)/2, MPC_RNDNN);
	complex_lngamma(w, z);

	mpc_set_d_d(z, (1-k1+k2+k3)/2, (rho1-rho2-rho3)/2, MPC_RNDNN);
	complex_lngamma(z, z);
	mpc_add(w, w, z, MPC_RNDNN);

	mpc_set_d_d(z, (1+k1-k2+k3)/2, -(rho1-rho2+rho3)/2, MPC_RNDNN);
	complex_lngamma(z, z);
	mpc_add(w, w, z, MPC_RNDNN);

	mpc_set_d_d(z, (1-k1-k2+k3)/2, (rho1+rho2-rho3)/2, MPC_RNDNN);
	complex_lngamma(z, z);
	mpc_add(w, w, z, MPC_RNDNN);

	mpc_exp(w, w, MPC_RNDNN);

	mpc_abs(x, w, MPC_RNDNN);
	mpc_div_fr(w, w, x, MPC_RNDNN);

	mpc_set_d_d(z, (1+k1+k2+k3)/2, -(rho1+rho2+rho3)/2, MPC_RNDNN);
	complex_lngamma(y, z);

	mpc_set_d_d(z, (1-k1-k2-k3)/2, -(rho1+rho2+rho3)/2, MPC_RNDNN);
	complex_lngamma(z, z);
	mpc_add(y, y, z, MPC_RNDNN);

	mpc_exp(y, y, MPC_RNDNN);
	mpc_mul(z, y, w, MPC_RNDNN );

	mpc_t kappa_res;
	mpc_init2(kappa_res, PRECISION);

	kappa ( kappa_res, two_j1, two_j2, two_j3,
					two_k1, two_k2, two_k3,
					two_rho1, two_rho2, two_rho3);
	//printf ("K %i %i %i %17g %17g \n",two_j1,two_j2,mpc_get_dc(kappa_res,MPC_RNDNN));
	mpc_mul (z,z,kappa_res,MPC_RNDNN );

	double complex result = phase*
                          sqrt(d(two_j1)*d(two_j2)*d(two_j3))*
													mpc_get_dc (z,MPC_RNDNN );

	mpc_clear(z);
	mpc_clear(y);
	mpc_clear(w);
	mpc_clear(kappa_res);
	mpfr_clear(x);

  return result;

}

double complex Xsimp (unsigned int two_j1, unsigned int two_j2, unsigned int two_j3,
                      unsigned int two_k1, unsigned int two_k2, unsigned int two_k3,
                      float Immirzi){
	double complex chi = X( two_j1, two_j2, two_j3,
                          two_k1, two_k2, two_k3,
                          Immirzi*two_k1, Immirzi*two_k2, Immirzi*two_k3);
  //printf ("C %17g %17g \n",chi);
  double complex result =  sqrt(d(two_j3))*chi;

  return result;
}

////////////////////////////////////////////////////////
////////////////////////////////////////////////////////
////////////////////////////////////////////////////////

double B3 ( unsigned int two_j1, unsigned int two_j2, unsigned int two_j3,
            unsigned int two_l1, unsigned int two_l2, unsigned int two_l3,
            float Immirzi){

	if (two_l1 < two_j1 || two_l2 < two_j2 || two_l3 < two_j3 ) return 0.0;

	// TODO B3 function requires to initialize complex gamma,
	// I initialize it globally to avoid parallelization problems
	// Find a better way of doing this

	//init_complex_gamma();

  double phase1 = real_negpow((int)(two_j1-two_j2+two_j3));
  double phase2 = real_negpow((int)(two_l1-two_l2+two_l3));

  double complex X1 = conj(Xsimp (two_j1, two_j2, two_j3,
                                  two_j1, two_j2, two_j3,
                                  Immirzi));
	//printf ("X1 %17g %17g \n",X1);

  double complex X2 = Xsimp ( two_l1, two_l2, two_l3,
                              two_j1, two_j2, two_j3,
                              Immirzi);
	//printf ("X2 %17g %17g \n",X2);

	mpc_t z,w;
	mpc_init2(z, PRECISION);
	mpc_init2(w, PRECISION);
	mpc_set_dc(z, X1, MPC_RNDNN);

	mpc_set_dc(w, X2, MPC_RNDNN);
	mpc_mul(z,z,w,MPC_RNDNN);

  double complex result = phase1*phase2*mpc_get_dc(z,MPC_RNDNN);
	//printf("%17g %17g \n",result);

	mpc_clear(z);
	mpc_clear(w);
	//clear_complex_gamma();

  return creal(result);


}

////////////////////////////////////////////////////////
////////////////////////////////////////////////////////
////////////////////////////////////////////////////////
