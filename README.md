We released an updated 2.0 version. It can be found at: https://github.com/qg-cpt-marseille/sl2cfoam

## Numerical methods for the EPRL spin foam transition amplitudes and Lorentzian recouping theory

The intricated combinatorial structure and the non-compactness of the group have always made the computation of SL(2,C) EPRL spin foam transition amplitudes a very hard and resource demanding task. With sl2cfoam we provide a C-coded library for the evaluation of the Lorentzian EPRL vertex amplitude.
We provide a tool to compute the Lorentzian EPRL 4-simplex vertex amplitude in the intertwiner basis and some utilities to evaluate SU(2) invariants, booster functions and SL(2,C) Clebsch-Gordan coefficients. We discuss the data storage, parallelizations, time and memory performances and possible future developments.

### Creating the library and compiling

Linux/Unix system supported. Few libraries are needed :

* Wigxjpf (http://fy.chalmers.se/subatom/wigxjpf/) to compute 3j,6j and 9j symbols. You should download the program and store it in the main directory of the library. Unmount the zip in the main directory of EPRL library, enter the folder and make it. We use version 1.7 of this code.

*	Gmp (https://gmplib.org/) to compute d-small Wigner matrices for coherent states and for booster function. We need arbitrary precision floating points.

* 	Mpfr (http://www.mpfr.org). As GMP, high precision library for floating points. Used in coherent states and boosters computations.

* 	Mpc (http://www.multiprecision.org/mpc/). As GMP and MPFR, high precision library for complex numbers. Used for booster functions.

* Gsl https://www.gnu.org/software/gsl/doc/html/intro.html to compute complex gamma functions in booster functions.

* OpenMp http://www.openmp.org/ to parallelize the computation.

To build the library you can use the make file:

* `make ` creates the library and compiles the test programs.
* `make test ` compiles only test programs.
* `./test ` launches test programs.

Then, to link the library to your program you can use `-lsl2cfoam` that is stored in lib/. Also, you have to link the main header src/library.
In testprg/ there are several test files that you can look at to learn how to use the library. You can compile your own program modifying:

gcc-7 -std=c99 -fopenmp -Iwigxjpf-1.7/inc/ -Isrc/ -Iinc/ test/yourprogram.c -Lwigxjpf-1.7/lib/ -Llib/ -lsl2cfoam -fopenmp -lgsl -lgslcblas -lmpfr -lmpc -lgmp -lwigxjpf -lm -lsl2cfoam -o bin/program

You also have to globally set the number of cores that you want to use while performing a computation. You can use the following command:

export OMP_NUM_THREADS=number_core

We are currently working to improve the 1.0 version. If you would like to report bugs or you have any question please feel free to contact sarno@cpt.univ-mrs.fr
At the present stage configurations involving a boundary spin j=0 seem to suffer a bug that we are trying to fix. 

###Licenses

sl2cfoam is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

sl2cfoam is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

If you use the library you should cite the paper in which we describe the library.

### How to use it

All the main functions are explained in the companion paper, describing the code and the main strategy. All functions are listed in the headers in inc/ if you need more details.

The main function is called FourSimplex(boundary spins, boundary intertwiners, delta L, Immirzi parameter), it computes a 4-simplex for given boundary data with the sum over the magnetic internal spins up to an homogeneous cut-off delta L. FourSimplex uses two main functions, B4_Hash and J15Symbol_Hash.

B4_Hash saves two different hash tables:

* The actual values of booster functions are saved in data/4simplex/Immirzi_𝛾/HashTablesBooster/two_j5.two_j6.two_j9.two_j10_two_Dl where two_j5.two_j6.two_j9.two_j10 are 2*the spins of the gauge fixed edge. We save date using the notation 2* spin in order to accomodate integers and half integers. Inside these tables there are all the combinations that we have already computed.  (same thing for the others tables).
Note that all combinations of intertwiners (allowed for given boundary spins) are stored when you use this function.
* In data/4simplex/aux/Immirzi_𝛾/HashTablesKeyBooster/two_j5.two_j6.two_j9.two_j10_two_Dl we save the information about which 4-simplices we have already computed in order to avoid repetitions and to save time. N.B It's important to have both the key and the data table while performing a computation. Not move one without the other.

J15Symbol_Hash saves also two different tables:

* It saves all the {6j} symbols needed for the computation in  data/4simplex/HashTableJ6/two_j5.two_j6.two_j9.two_j10_two_i4_two_Dl where two_i4 is the intertwiner of the gauge fixed edge as in paper's notation.
* In data/4simplex/aux/HashTablesKeyJ15/two_j5.two_j6.two_j9.two_j10_two_i4_two_Dl we save the information about which 4-simplices we have already computed.

When the function is sure to have all the ingredients, it starts the computation parallelizing the summation over the magnetic spins l and it saves data in an hash table in data/4simplex/Immirzi_𝛾/HashTablesEPRL/two_j5.two_j6.two_j9.two_j10_two_Dl.

You can glue more 4-simplices, we suggest to always have the gauge fixed edge as a boundary one and not as a bulk one in order to avoid proliferation of hash table files.

We provide several test files that you can look at to see how to use the program and also to adapt the present code to your own necessities. We provide also functions to compute single B4 functions, coherent states and SL(2,C) recoupling theory tools.

### Main Headers

* 	khash.h to create hash tables. It has been modified from https://github.com/attractivechaos/klib/blob/master/khash.h and from https://github.com/attractivechaos/klib/issues/44 in order to accomodate array as keys and double complex as values. Write function has been taken from https://github.com/attractivechaos/klib/pull/76/commits/494da2ec77e4059bf3a5ca8561a1763e2a6bc2ad.
* 	jsymbols.h to assign, given a certain combination of entries, a “key” to a 6j symbol. We modified a function from the wigxjpf library. In this header we also define functions to compute hash tables for different {nj} symbols, to store results and to compute {15j} symbols.
* 	b4function.h.  The B4 functions that we are using are computed starting from François Collet's formula and using his code modified from C++ to C. His paper about the formula and with the code is about to appear. Inside this file you can also find new functions to hash the boosters data needed for a certain computation.
* amplitude.h . Here we define the main function FourSimplex.
* coherentstates.h . To use Livine-Speziale coherent intertwiners.
* recouplingsl2c.h . To look at sl2c {3j} and {6j}.
* config.h . In this header some library parameters are set.

We want to thank very much François Collet  and Francesco Gozzini whom provided the B4 function and complex gamma function in arbitrary precision floating points. We also to thank them for many helpful discussions.
