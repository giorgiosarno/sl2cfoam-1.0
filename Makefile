QUIET ?= @

CC = gcc-7
AR = ar
RLIB = ranlib

CFLAGS = -std=c99 -fopenmp -fPIC -g -Iwigxjpf-1.7/inc/ -Isrc/
LDFLAGS = -Lwigxjpf-1.7/lib/
LDLIBS =  -fopenmp -lgsl -lgslcblas -lmpfr -lmpc -lgmp -lwigxjpf -lm

SRCDIR = src
OBJDIR = obj
INCDIR = inc
LIBDIR = lib
BINDIR = bin
TESTDIR = testprg

default: lib test
all: default

INCS = src/utilities.h inc/library.h src/jsymbols.h src/khash.h src/common.h src/b4function.h src/amplitude.h src/config.h src/coherentstates.h src/cgamma.h src/b3function.h src/recouplingsl2c.h
_OBJS = utilities.o khash.o library.o jsymbols.o coherentstates.o common.o b4function.o amplitude.o config.o b3function.o cgamma.o recouplingsl2c.o
OBJS = $(patsubst %,$(OBJDIR)/%,$(_OBJS))

# library/src object files
$(OBJDIR)/%.o: $(SRCDIR)/%.c $(INCS)
	@echo "   CC    $@"
	$(QUIET)mkdir -p $(dir $@)
	$(QUIET)$(CC) $(CFLAGS) -c -o $@ $<

# library/src object files
$(OBJDIR)/%.o: $(INCDIR)/%.h $(INCS)
	@echo "   CC    $@"
	$(QUIET)mkdir -p $(dir $@)
	$(QUIET)$(CC) $(CFLAGS) -c -o $@ $<

# hash/inc object files
$(OBJDIR)/%.o: $(SRCDIR)/%.h $(INCS)
	@echo "   CC    $@"
	$(QUIET)mkdir -p $(dir $@)
	$(QUIET)$(CC) $(CFLAGS) -c -o $@ $<


# Built Library
$(LIBDIR)/libsl2cfoam.a: $(OBJS)
	@echo "   AR    $@"
	$(QUIET)mkdir -p $(dir $@)
	$(QUIET)$(AR) $(ARFLAGS) $@ $(OBJS)
	$(QUIET)$(RLIB) $@

# test binary files
$(OBJDIR)/%.o: testprg/%.c $(INCS)
	@echo "   CC    $@"
	$(QUIET)mkdir -p $(dir $@)
	$(QUIET)$(CC) $(CFLAGS) -c -o $@ $< -I$(SRCDIR)/ -I$(INCDIR)/

# Compile Test Programs
$(BINDIR)/3simplextest: $(LIBDIR)/libsl2cfoam.a $(OBJDIR)/3simplextest.o
	@echo "   CC    $@"
	$(QUIET)mkdir -p $(dir $@)
	$(QUIET)$(CC) $(OBJDIR)/3simplextest.o $(LDFLAGS) -Llib/ -lsl2cfoam $(LDLIBS) -lsl2cfoam -o $@

$(BINDIR)/4simplextest: $(LIBDIR)/libsl2cfoam.a $(OBJDIR)/4simplextest.o
	@echo "   CC    $@"
	$(QUIET)mkdir -p $(dir $@)
	$(QUIET)$(CC) $(OBJDIR)/4simplextest.o $(LDFLAGS) -Llib/ -lsl2cfoam $(LDLIBS) -lsl2cfoam -o $@

$(BINDIR)/b3test: $(LIBDIR)/libsl2cfoam.a $(OBJDIR)/b3test.o
	@echo "   CC    $@"
	$(QUIET)mkdir -p $(dir $@)
	$(QUIET)$(CC) $(OBJDIR)/b3test.o $(LDFLAGS) -Llib/ -lsl2cfoam $(LDLIBS) -lsl2cfoam -o $@

$(BINDIR)/recouplingtest: $(LIBDIR)/libsl2cfoam.a $(OBJDIR)/recouplingtest.o
	@echo "   CC    $@"
	$(QUIET)mkdir -p $(dir $@)
	$(QUIET)$(CC) $(OBJDIR)/recouplingtest.o $(LDFLAGS) -Llib/ -lsl2cfoam $(LDLIBS) -lsl2cfoam -o $@

$(BINDIR)/b4test: $(LIBDIR)/libsl2cfoam.a $(OBJDIR)/b4test.o
	@echo "   CC    $@"
	$(QUIET)mkdir -p $(dir $@)
	$(QUIET)$(CC) $(OBJDIR)/b4test.o $(LDFLAGS) -Llib/ -lsl2cfoam $(LDLIBS) -lsl2cfoam -o $@

.PHONY: default all clean

lib: $(LIBDIR)/libsl2cfoam.a
test: $(BINDIR)/3simplextest $(BINDIR)/4simplextest $(BINDIR)/b3test $(BINDIR)/b4test $(BINDIR)/recouplingtest

clean:
	rm -rf $(OBJDIR)
	rm -rf $(LIBDIR)
	rm -rf $(BINDIR)
